﻿using System.Collections.Generic;

namespace PixelBucketWorld.Services.Helpers
{
    public class PagedResult<T>
    {
        public List<T> CurrentResults { get; set; }

        public int CollectionCount { get; set; }
    }
}
