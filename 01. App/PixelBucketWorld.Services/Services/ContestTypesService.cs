﻿using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.Services.Services
{
    public class ContestTypesService : GenericService<ContestType>
    {
        public ContestTypesService(PBWDbContext context)
            : base(context)
        {

        }

    }
}
