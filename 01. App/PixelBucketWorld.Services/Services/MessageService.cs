﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Consts.Strings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class MessageService : GenericService<InvitationalMessage>
    {

        public MessageService(PBWDbContext context)
            : base(context)
        {

        }

        public async Task<IEnumerable<InvitationalMessage>> GetAsync(int? userId = null, int? contestId = null)
        {
            IQueryable<InvitationalMessage> invMessages = this.dbSet
                .Include(i => i.Receiver)
                .Include(i => i.Sender)
                .Include(i => i.Contest);

            if (userId != null)
            {
                invMessages = invMessages.Where(m => m.ReceiverId == userId);
            }

            if (contestId != null)
            {
                invMessages = invMessages.Where(m => m.ContestId == contestId);
            }

            return await invMessages.ToListAsync();
        }

        public async Task<IEnumerable<InvitationalMessage>> GetUserInvitationalMessagesAsync(int userId)
        {
            return await this.dbSet.Where(im => im.ReceiverId == userId).ToListAsync();
        }

        public override async Task<InvitationalMessage> GetByIdAsync(int id)
        {
            var message = await base.GetByIdAsync(id);

            if (message == null)
            {
                throw new ArgumentNullException();
            }

            var entry = base.context.Entry(message);

            entry.Reference(m => m.Receiver).Load();
            entry.Reference(m => m.Sender).Load();
            entry.Reference(m => m.Contest).Query().Include(c=>c.CurrentPhase).Include(c=>c.JurorsLink).Load();

            return message;
        }

        public async Task SendParticipantsInvitationalMessageAsync(IEnumerable<int> userIdsToInvite, int contestId, int senderId)
        {
            if (userIdsToInvite == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var userId in userIdsToInvite)
            {
                var invitationalMessage = await this.CreateParticipationInvitationalMessageAsync(userId, contestId, senderId);
            }
        }

        public async Task SendJuryInvitationalMessageAsync(IEnumerable<int> userIdsForJury, int contestId, int senderId)
        {
            if (userIdsForJury == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var userId in userIdsForJury)
            {
                var invitationalMessage = await this.CreateJuryInvitationalMessageAsync(userId, contestId, senderId);
            }
        }

        public async Task<InvitationalMessage> CreateParticipationInvitationalMessageAsync(int userId, int contestId, int senderId)
        {
            // validate receiver

            var user = await base.context.Users.FindAsync(userId);

            if (user == null)
            {
                throw new ArgumentNullException();
            }
            if (user.RankId == null)
            {
                throw new InvalidOperationException("Can't send invitation to organizer.");
            }

            // validate contest

            var contest = await base.context.Contests.FindAsync(contestId);

            if (contest == null)
            {
                throw new ArgumentNullException();
            }
            if (contest.TypeId != ContestTypeConsts.INVITATIONAL)
            {
                throw new InvalidOperationException("Contest is not invitational. Can'send invitation.");
            }
            if (contest.PhaseId != PhaseConsts.PRESTART)
            {
                throw new InvalidOperationException("Contest is not in phase prestart. Time for sending invitations has passed.");
            }

            // check for invitation duplication
            var participantLink = await this.context.ContestParticipantLinks.FindAsync(userId, contestId);
            if (participantLink != null)
            {
                throw new InvalidOperationException("Duplicate inivitations.");
            }

            var newMessage = new InvitationalMessage()
            {
                Title = string.Format(InvitationMessagesConsts.TITLE_PARTICIPANT),
                Content = string.Format(InvitationMessagesConsts.CONTENT_PARTICIPANT, user.UserName, contest.Name),
                ContestId = contestId,
                ReceiverId = userId,
                SenderId = senderId,
                InvitationDate = DateTime.Now
            };

            this.dbSet.Add(newMessage);

            return newMessage;

        }

        public async Task<InvitationalMessage> CreateJuryInvitationalMessageAsync(int userId, int contestId, int senderId)
        {
            // validate user
            var user = await this.context.Users.FindAsync(userId);
            if (user == null)
            {
                throw new ArgumentNullException();
            }
            if (user.RankId == null)
            {
                throw new InvalidOperationException("Can't send invitation to organizer.");
            }
            if (user.RankId != RankConsts.MASTER && user.RankId != RankConsts.WISE_AND_BENEVOLENT_PHOTO_DICTATOR)
            {
                throw new InvalidOperationException("The receiver rank is too low to participate in jury.");
            }

            // validate contest
            var contest = await this.context.Contests.FindAsync(contestId);
            if (contest == null)
            {
                throw new ArgumentNullException();
            }
            if (contest.PhaseId != PhaseConsts.PRESTART)
            {
                throw new InvalidOperationException("Contest is not in phase prestart. Time for sending invitations has passed.");
            }

            // check for invitation duplication
            var jurorLink = await this.context.ContestJurorLinks.FindAsync(userId, contestId);
            if (jurorLink != null)
            {
                throw new InvalidOperationException("Duplicate inivitations.");
            }

            var newMessage = new InvitationalMessage()
            {
                Title = InvitationMessagesConsts.TITLE_JUROR,
                Content = string.Format(InvitationMessagesConsts.CONTENT_JUROR, user.UserName, contest.Name),
                ContestId = contestId,
                ReceiverId = userId,
                SenderId = senderId,
                InvitationDate = DateTime.Now
            };

            this.dbSet.Add(newMessage);

            return newMessage;
        }

        public async Task<InvitationalMessage> AcceptParticipantInvitation(int invId, int userId)
        {
            var invitation = await this.dbSet.FindAsync(invId);
            if (invitation == null)
            {
                throw new ArgumentNullException(string.Format(ErrorConsts.ENTITY_NOT_FOUND, "Invitation", invId));
            }

            if (invitation.ReceiverId != userId)
            {
                throw new InvalidOperationException("It's not yours invitation. Can't accept it.");
            }

            invitation.ContestAccepted = true;
            invitation.HasBeenViewed = true;

            return invitation;
        }

        public async Task<InvitationalMessage> AcceptJurorInvitationAsync(int invId, int userId)
        {
            var invitation = await this.dbSet.FindAsync(invId);
            if (invitation == null)
            {
                throw new ArgumentNullException(string.Format(ErrorConsts.ENTITY_NOT_FOUND, "Invitation", invId));
            }

            if (invitation.ReceiverId != userId)
            {
                throw new InvalidOperationException("It's not yours invitation. Can't accept it.");
            }

            var photo = this.context.Photos.FirstOrDefault(p => p.UserId == userId && p.ContestId == invitation.ContestId);
            if (photo != null)
            {
                throw new InvalidOperationException("You have uploaded photo and can't accept the invitation.");
            }

            invitation.ContestAccepted = true;
            invitation.HasBeenViewed = true;

            return invitation;
        }

    }
}
