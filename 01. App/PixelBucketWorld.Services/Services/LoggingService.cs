﻿using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models.Logging;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Services.Contracts;
using System;
using PixeBucketWorld.Data.Models.Logging.Enums;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class LoggingService : ILoggingService
    {
        private readonly PBWDbContext context;

        public LoggingService(PBWDbContext context)
        {
            this.context = context;
        }

        public async Task LogInformationAsync<T>(InformationLogEvent logEvent, string detailedMessage, T model)
        {
            if (model == null)
            {
                throw new ArgumentNullException();
            }

            var modelDetails = GetModelDetails(model);
            var message = string.Empty;

            switch (logEvent)
            {
                case InformationLogEvent.INSERT_ITEM:
                    message = string.Format(OperationConsts.ENTITY_CREATED, modelDetails.typeName, modelDetails.modelId);
                    break;
                case InformationLogEvent.UPDATE_ITEM:
                    message = string.Format(OperationConsts.ENTITY_UPDATED, modelDetails.typeName, modelDetails.modelId);
                    break;
                case InformationLogEvent.DELETE_ITEM:
                    message = string.Format(OperationConsts.ENTITY_DELETED, modelDetails.typeName, modelDetails.modelId);
                    break;
                default:
                    throw new InvalidOperationException();
            }

            var logEntry = new LogEntry()
            {
                LogEventId = (int)logEvent,
                LogLevel = LogLevelConsts.INFORMATION,
                Message = message,
                Details = detailedMessage,
                LogTime = DateTime.Now
            };

            await context.Logs.AddAsync(logEntry);
            await context.SaveChangesAsync();
        }

        public async Task LogWarningAsync<T>(WarningLogEvent logEvent, string detailedMessage, T model)
        {
            if (model == null)
            {
                throw new ArgumentNullException();
            }

            var modelDetails = GetModelDetails(model);
            var message = string.Empty;

            switch (logEvent)
            {
                case WarningLogEvent.GET_ITEM_NOT_FOUND:
                    message = string.Format(OperationConsts.ENTITY_NOT_FOUND, modelDetails.typeName, modelDetails.modelId);
                    break;
                case WarningLogEvent.UPDATE_ITEM_NOT_FOUND:
                    message = string.Format(OperationConsts.ENTITY_NOT_FOUND, modelDetails.typeName, modelDetails.modelId);
                    break;
                default:
                    throw new InvalidOperationException();
            }

            var logEntry = new LogEntry()
            {
                LogEventId = (int)logEvent,
                LogLevel = LogLevelConsts.WARNING,
                Message = message,
                Details = detailedMessage,
                LogTime = DateTime.Now
            };

            await context.Logs.AddAsync(logEntry);
            await context.SaveChangesAsync();
        }

        public async Task LogErrorAsync(Exception e)
        {
            if(e == null)
            {
                throw new ArgumentNullException();
            }

            var excLogEntry = new LogEntry()
            {
                Details = e.StackTrace,
                LogLevel = LogLevelConsts.ERROR,
                Message = e.Message,
                LogTime = DateTime.Now
            };

            await context.Logs.AddAsync(excLogEntry);
            await context.SaveChangesAsync();
        }

        private (string modelId, string typeName) GetModelDetails<T>(T model)
        {
            var modelId = model.GetType().GetProperty("Id").GetValue(model).ToString();
            var typeName = model.GetType().Name;

            return (modelId, typeName);
        }
    }
}
