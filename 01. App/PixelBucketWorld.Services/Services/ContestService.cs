﻿using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Consts.Strings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services.Services
{
    public class ContestService : GenericService<Contest>
    {
        public ContestService(PBWDbContext context)
            : base(context)
        {

        }

        public async Task<IEnumerable<Contest>> GetContestsByPhaseAndTypeAsync(int? typeId = null, int? phaseId = null)
        {
            IQueryable<Contest> contests = this.dbSet
                .Include(c => c.Category)
                .Include(c => c.CurrentPhase)
                .Include(c => c.Type);

            if (typeId != null)
            {
                contests = contests.Where(c => c.TypeId == typeId);
            }

            if (phaseId != null)
            {
                contests = contests.Where(c => c.PhaseId == phaseId);
            }

            return await contests.ToListAsync();
        }

        //        Active contests
        //          phase pre + phase I
        //          Open + invitational(not accepted)

        public async Task<IEnumerable<Contest>> GetActiveOpenContestsAsync2(int? excludeUserId = null)
        {
            var contests = this.context.Contests
                .Include(c => c.CurrentPhase)
                .Include(c => c.Category)
                .Include(c => c.Type)
                .Where(c => c.CurrentPhase.Id <= PhaseConsts.PHASE_ONE);

            if (excludeUserId.HasValue)
            {
                contests = contests.Include(c => c.ParticipantsLink)
                    .Where(c => c.ParticipantsLink.All(cpl => cpl.UserId != excludeUserId) || c.ParticipantsLink.Any(cpl => cpl.UserId == excludeUserId && (cpl.ParticipationDate == null || cpl.UploadingPhotoTime == null)));
            }

            ///checking if some of the contests are not already accepted as jury 

            return await contests.ToListAsync();
        }

        public async Task<IEnumerable<Contest>> GetParticipantOnGoingContestAsync(int userId)
        {
            var contests = this.context.ContestParticipantLinks
                .Include(cpc => cpc.Contest)
                .Include(cpc => cpc.Contest.Category)
                .Include(cpc => cpc.Contest.CurrentPhase)
                .Include(cpc => cpc.Contest.Type)
                .Where(cpc => cpc.UserId == userId && cpc.Contest.PhaseId != PhaseConsts.FINISHED && cpc.UploadingPhotoTime != null) //IsAccepted could be null in case of Open contest
                .Select(cpc => cpc.Contest);

            return await contests.ToListAsync();
        }

        //        My Finished Contests
        //          phase F
        //          Open + invitational(accepted)
        public async Task<IEnumerable<Contest>> GetParticipantFinishedContestAsync(int userId)
        {
            var contests = this.context.ContestParticipantLinks
                .Include(cpc => cpc.Contest)                    
                .Include(cpc => cpc.Contest.Category)
                .Include(cpc => cpc.Contest.CurrentPhase)
                .Include(cpc => cpc.Contest.Type)
                .Where(cpc => cpc.UserId == userId && cpc.Contest.PhaseId == PhaseConsts.FINISHED && cpc.UploadingPhotoTime != null) //IsAccepted could be null in case of Open contest
                .Select(cpc => cpc.Contest);

            return await contests.ToListAsync();
        }

        // this could be userd for every contest phase
        public async Task<IEnumerable<Contest>> GetParticipantContestsAsync(int userId, int? phaseId = null) // XXX
        {
            var contestParticipantLinks = this.context.ContestParticipantLinks
                .Include(cpc => cpc.Contest)
                .Include(cpc => cpc.Contest.Category)
                .Include(cpc => cpc.Contest.CurrentPhase)
                .Include(cpc => cpc.Contest.Type)
                .Where(cpc => cpc.UserId == userId);

            if (phaseId != null)
            {
                contestParticipantLinks = contestParticipantLinks.Where(cpc => cpc.Contest.PhaseId == phaseId);
            }

            var contests = await contestParticipantLinks.Select(cpc => cpc.Contest).ToListAsync();

            return contests;
        }

        public async Task<IEnumerable<Contest>> GetJurorOnGoingContests(int userId)
        {
            var contests = this.context.ContestJurorLinks
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.CurrentPhase)
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.Category)
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.Type)
                            .Where(cjl => cjl.UserId == userId && cjl.IsAccepted)
                            .Select(cjl => cjl.Contest)
                            .Where(c => c.CurrentPhase.Id != PhaseConsts.FINISHED);

            return await contests.ToListAsync();
        }

        public async Task<IEnumerable<Contest>> GetJurorActiveContests(int userId)
        {
            var contests = this.context.ContestJurorLinks
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.CurrentPhase)
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.Category)
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.Type)
                            .Where(cjl => cjl.UserId == userId && !cjl.IsAccepted)
                            .Select(cjl => cjl.Contest)
                            .Where(c => c.CurrentPhase.Id < PhaseConsts.TRANSITION);


            return await contests.ToListAsync();
        }

        public async Task<IEnumerable<Contest>> GetJurorFinishedContests(int userId)
        {
            var contests = this.context.ContestJurorLinks
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.CurrentPhase)
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.Category)
                            .Include(cjl => cjl.Contest)
                                .ThenInclude(c => c.Type)
                            .Where(cjl => cjl.UserId == userId && cjl.IsAccepted)
                            .Select(cjl => cjl.Contest)
                            .Where(c => c.CurrentPhase.Id == PhaseConsts.FINISHED);

            return await contests.ToListAsync();
        }

        public async override Task<Contest> GetByIdAsync(int id)
        {
            var contest = await base.GetByIdAsync(id);

            if (contest == null)
            {
                throw new ArgumentNullException();
            }

            this.context.Entry(contest).Reference(c => c.Type).Load();

            this.context.Entry(contest).Reference(c => c.Category).Load();

            this.context.Entry(contest).Reference(c => c.CurrentPhase).Load();

            this.context.Entry(contest).Collection(c => c.ParticipantsLink)
                .Query()
                .Include(pl => pl.User)
                    .ThenInclude(u => u.Rank)
                .Load();

            this.context.Entry(contest).Collection(c => c.JurorsLink).Query().Include(jl => jl.User).Load();

            this.context.Entry(contest).Collection(c => c.UploadedPhotos)
                .Query()
                .Include(up => up.PhotoAssessments)
                .Include(up => up.User).Load();

            return contest;
        }

        public override async Task<Contest> CreateAsync(Contest entity)
        {
            var contest = await this.dbSet.FirstOrDefaultAsync(c => c.Name == entity.Name);

            if (contest != null)
            {
                throw new DuplicateNameException(ErrorConsts.ENTITY_EXISTS);
            }

            if (await CategoryIdIsInvalid(entity))
            {
                throw new ArgumentOutOfRangeException("Unexisting category!");
            }

            if (await ContestTypeIdIsInvalid(entity))
            {
                throw new ArgumentOutOfRangeException("Unexisting contest type!");
            }

            if (entity.PhaseId != PhaseConsts.PRESTART)
            {
                throw new ArgumentOutOfRangeException("Context pahseId must be 1 - prestart.");
            }

            await this.dbSet.AddAsync(entity);

            this.context.Entry(entity).Reference(c => c.CurrentPhase).Load();
            this.context.Entry(entity).Reference(c => c.Type).Load();
            this.context.Entry(entity).Reference(c => c.Category).Load();

            return entity;
        }


        public async Task<List<Contest>> SwitchPhasesAsync(DateTime currnetTime)
        {
            var activeContests = await this.dbSet
                .Where(c => c.PhaseId != PhaseConsts.FINISHED)
                .ToListAsync();

            List<Contest> switchedContests = new List<Contest>();

            foreach (var contest in activeContests)
            {
                if (
                (contest.PhaseId == PhaseConsts.PRESTART && currnetTime > contest.PhaseOneStartingDate) ||
                (contest.PhaseId == PhaseConsts.PHASE_ONE && currnetTime > contest.PhaseOneFinishingDate) ||
                (contest.PhaseId == PhaseConsts.TRANSITION && currnetTime > contest.PhaseTwoStartingDate) ||
                (contest.PhaseId == PhaseConsts.PHASE_TWO && currnetTime > contest.PhaseTwoFinishingDate))
                {
                    contest.PhaseId++;
                    switchedContests.Add(contest);
                }
            }

            return switchedContests;
        }

        public async Task FinalizeAsync(List<Contest> finishedContests)
        {
            foreach (var contest in finishedContests)
            {
                await LoadContest(contest);

                if (contest.UploadedPhotos.Any())
                {
                    this.RatePhotos(contest.UploadedPhotos, contest.JurorsLink);

                    var classifiedPhotos = this.GetClassifiedPhotos(contest.UploadedPhotos);
                    var contestWinners = this.GetContestWinners(classifiedPhotos).ToList();

                    await this.AssignWinningPoints(contestWinners);
                    this.AssignParticipationPoints(contest.TypeId, contest.UploadedPhotos);
                    await this.UpdateParticipantsRanks(contest.UploadedPhotos);
                }            
            }
        }

        private async Task LoadContest(Contest contest)
        {
            await this.context.Entry(contest)
                .Collection(c => c.JurorsLink)
                .LoadAsync();
            await this.context.Entry(contest)
                .Collection(c => c.UploadedPhotos)
                .Query()
                .Include(p => p.PhotoAssessments)
                .Include(p => p.User)
                    .ThenInclude(u => u.Rank)
                .LoadAsync();
        }

        private void RatePhotos(IEnumerable<Photo> participantsPhotos, IEnumerable<ContestJurorLink> jurorLinks)
        {
            foreach (var photo in participantsPhotos)
            {
                var assesmenCount = photo.PhotoAssessments.Count();
                var persistentJurorsCount = this.context.Users.Where(u => u.RankId == null).Count();
                var participantJurorsCount = jurorLinks.Where(jl => jl.IsAccepted).Count();
                var jurorsCount = persistentJurorsCount + participantJurorsCount;
                int addPoints = 0;

                if (assesmenCount < jurorsCount)
                {
                    addPoints = (jurorsCount - assesmenCount) * 3;
                }

                var averagePoints = (double)((photo.PhotoAssessments.Sum(pa => pa.Score) + addPoints)) / jurorsCount;
                photo.FinalPoints += averagePoints;
            }
        }

        private IEnumerable<IGrouping<double, Photo>> GetClassifiedPhotos(IEnumerable<Photo> participantsPhotos)
        {
            return participantsPhotos.OrderByDescending(p => p.FinalPoints).GroupBy(p => p.FinalPoints);
        }

        private IEnumerable<IGrouping<double, Photo>> GetContestWinners(IEnumerable<IGrouping<double, Photo>> classifiedPhotos)
        {
            return classifiedPhotos.Take(3);
        }

        private async Task AssignWinningPoints(List<IGrouping<double, Photo>> photoWinners)
        {
            var winnerMaxPoints = 50;
            var maxRankMaxRankingPoints = this.context.Ranks.Max(r => r.RequiredPoints);
            var maxRank = await this.context.Ranks.FirstOrDefaultAsync(r => r.RequiredPoints == maxRankMaxRankingPoints);

            var firstPlaceWinner = photoWinners[0].First();
            var continueFromGroup = 0;

            if (photoWinners.Count > 1)
            {
                var secondPlaceWinner = photoWinners[1].First();

                //Assigning points for photo classification to users
                if ((firstPlaceWinner.FinalPoints / secondPlaceWinner.FinalPoints) >= 2)
                {
                    continueFromGroup++;
                    var firstPlaceWinners = photoWinners[0]; //foreach all winners

                    foreach (var photo in firstPlaceWinners)
                    {
                        photo.User.CurrentRankingPoints += 75;
                        photo.ContestRanking = continueFromGroup;
                    }

                    winnerMaxPoints -= 15;
                }
            }

            for (int winningPlace = continueFromGroup; winningPlace < photoWinners.Count; winningPlace++)
            {
                continueFromGroup++;
                var currGroup = photoWinners[winningPlace];

                if (currGroup.Count() > 1)
                {
                    var sharedPoints = winnerMaxPoints - 10;

                    foreach (var photo in currGroup)
                    {
                        photo.User.CurrentRankingPoints += sharedPoints;
                        photo.ContestRanking = continueFromGroup;
                    }
                }
                else
                {
                    var photo = currGroup.First();

                    photo.User.CurrentRankingPoints += winnerMaxPoints;
                    photo.ContestRanking = continueFromGroup;
                }

                winnerMaxPoints -= 15;
            }
        }

        private void AssignParticipationPoints(int contestTypeId, IEnumerable<Photo> contestPhotos)
        {
            if (contestTypeId == ContestTypeConsts.OPEN)
            {
                foreach (var photo in contestPhotos)
                {
                    photo.User.CurrentRankingPoints += 1;
                }
            }
            else
            {
                foreach (var photo in contestPhotos)
                {
                    photo.User.CurrentRankingPoints += 3;
                }
            }
        }

        private async Task UpdateParticipantsRanks(IEnumerable<Photo> contestPhotos)
        {
            foreach (var photo in contestPhotos)
            {
                var userRank = photo.User.RankId;

                if (userRank != RankConsts.WISE_AND_BENEVOLENT_PHOTO_DICTATOR)
                {
                    var nextRank = await this.context.Ranks.FindAsync(photo.User.RankId + 1);
                    var currentUserPoints = photo.User.CurrentRankingPoints;

                    if (currentUserPoints >= nextRank.RequiredPoints)
                    {
                        photo.User.RankId++;
                    }
                }

            }
        }

        private async Task<bool> CategoryIdIsInvalid(Contest entity)
        {
            var category = await this.context.Set<Category>().FirstOrDefaultAsync(e => e.Id == entity.CategoryId);

            if (category == null)
            {
                return true;
            }

            return false;
        }

        private async Task<bool> ContestTypeIdIsInvalid(Contest entity)
        {
            var category = await this.context.Set<Category>().FirstOrDefaultAsync(e => e.Id == entity.CategoryId);

            if (category == null)
            {
                return true;
            }

            return false;
        }

    }
}