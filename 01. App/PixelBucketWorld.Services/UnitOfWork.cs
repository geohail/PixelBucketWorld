﻿using PixeBucketWorld.Data;
using PixelBucketWorld.Services.Services;
using System;
using System.Threading.Tasks;

namespace PixelBucketWorld.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PBWDbContext context;
        private PhotoService photoService;
        private ContestService contestService;
        private UserService userService;
        private CategoryService categoryService;
        private MessageService messageService;
        private RankService rankService;
        private RoleService roleService;
        private PhotoAssesmentService photoAssessmentService;
        private ContestParticipantLinkService contestParticipantLinkService;
        private ContestJurorLinkService contestJurorLinkService;
        private ContestTypesService contestTypesService;

        public UnitOfWork(PBWDbContext context)
        {
            this.context = context;
        }

        public PhotoService PhotoService
        {
            get
            {
                if (this.photoService == null)
                {
                    this.photoService = new PhotoService(this.context);
                }
                return this.photoService;
            }
        }

        public ContestService ContestService
        {
            get
            {
                if (this.contestService == null)
                {
                    this.contestService = new ContestService(context);
                }

                return this.contestService;
            }
        }

        public UserService UserService
        {
            get
            {
                if (this.userService == null)
                {
                    this.userService = new UserService(context, roleService);
                }
                return this.userService;
            }
        }

        public CategoryService CategoryService
        {
            get
            {
                if (this.categoryService == null)
                {
                    this.categoryService = new CategoryService(context);
                }
                return this.categoryService;
            }
        }

        public MessageService MessageService
        {
            get
            {
                if(this.messageService == null)
                {
                    this.messageService = new MessageService(context);
                }

                return messageService;
            }
        }

        public RankService RankService
        {
            get
            {
                if(this.rankService == null)
                {
                    this.rankService = new RankService(context);
                }

                return rankService;
            }
        }

        public RoleService RoleService
        {
            get
            {
                if (this.roleService == null)
                {
                    this.roleService = new RoleService(context);
                }

                return roleService;
            }
        }

        public PhotoAssesmentService PhotoAssessmentService
        {
            get
            {
                if(this.photoAssessmentService == null)
                {
                    this.photoAssessmentService = new PhotoAssesmentService(context);
                }

                return photoAssessmentService;
            }
        }

        public ContestParticipantLinkService ContestParticipantLinkService
        {
            get
            {
                if (this.contestParticipantLinkService == null)
                {
                    this.contestParticipantLinkService = new ContestParticipantLinkService(context);
                }

                return contestParticipantLinkService;
            }
        }

        public ContestJurorLinkService ContestJurorLinkService
        {
            get
            {
                if (this.contestJurorLinkService == null)
                {
                    this.contestJurorLinkService = new ContestJurorLinkService(context);
                }

                return contestJurorLinkService;
            }
        }

        public ContestTypesService ContestTypesService
        {
            get
            {
                if (this.contestTypesService == null)
                {
                    this.contestTypesService = new ContestTypesService(context);
                }

                return contestTypesService;
            }
        }

        public async Task SaveAsync()
        {
            try
            {
                await this.context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                throw new InvalidOperationException(e.InnerException.Message);
            }
        }
    }
}
