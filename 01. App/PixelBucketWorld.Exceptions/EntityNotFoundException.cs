﻿using System;

namespace PixelBucketWorld.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string entityType, int id) : base(string.Format(errorstrings)
        {

        }
    }
}
