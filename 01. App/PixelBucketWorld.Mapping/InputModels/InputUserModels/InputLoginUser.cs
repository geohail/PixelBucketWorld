﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PixelBucketWorld.Map.InputModels.InputUserModels
{
    public class InputLoginUser
    {
        public string Password { get; set; }
        public string Username { get; set; }
    }
}
