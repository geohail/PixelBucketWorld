﻿namespace PixelBucketWorld.Consts.Strings
{
    public static class ErrorConsts
    {
        public const string ENTITY_NOT_FOUND = "{0} with {1} has not been found";

        public const string ENTITY_EXISTS = "Entity with the same name exists";
    }
}
