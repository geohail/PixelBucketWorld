﻿namespace PixelBucketWorld.Consts.Strings
{
    public static class OperationConsts
    {
        public const string ENTITY_UPDATED = "{0} {1} has been updated";
        public const string ENTITY_PROPERTY_UPDATED = "Property {0} has changed from {1} to {2}";
        public const string ENTITY_CREATED = "{0} {1} has been created";
        public const string ENTITY_DELETED = "{0} {1} has been deleted";
        public const string ENTITY_NOT_FOUND = "{0} with {1} has not been found";
    }
}
