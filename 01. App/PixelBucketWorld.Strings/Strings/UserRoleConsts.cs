﻿namespace PixelBucketWorld.Consts.Strings
{
    public static class UserRoleConsts
    {
        public const string ORGANISER = "Organiser";
        public const string PARTICIPANT = "Participant";
    }

}
