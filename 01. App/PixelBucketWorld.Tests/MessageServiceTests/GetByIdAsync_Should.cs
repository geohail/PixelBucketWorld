﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.MessageServiceTests
{
    [TestClass]
    class GetByIdAsync_Should
    {
        [TestMethod]
        public async Task GetByIdAsync_ShouldReturnEmptyCollection_IfValidMessageIdIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act
                var message = await unitOfWork.MessageService.GetByIdAsync(1);

                //Assert
                Assert.IsTrue(message.Id == 1);
            }
        }

        [TestMethod]
        public async Task GetByIdAsync_ShouldReturnNull_IfInvalidMessageIdPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                //Arrange
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act && Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await unitOfWork.MessageService.GetByIdAsync(1000));
            }
        }
    }
}
