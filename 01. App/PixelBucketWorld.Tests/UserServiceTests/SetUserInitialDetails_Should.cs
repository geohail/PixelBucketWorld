﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.UserServiceTests
{
    [TestClass]
    public class SetUserInitialDetails_Should
    {
        [TestMethod]
        public void SetUserInitialDetails_ShouldSetCorrectInitialDetails_WhenUserIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var user = new User();
                var unitOfWork = new UnitOfWork(context);

                //Act
                unitOfWork.UserService.SetUserInitialDetails(user);

                //Assert
                Assert.IsTrue(user.RankId == 1);
                Assert.IsTrue(user.CurrentRankingPoints == 0);
            }
        }

        [TestMethod]
        public void SetUserInitialDetails_ShouldSetCorrectInitialDetails_WhenNullIsPassed()
        {
            var options = Util.GetDbContextOptions(StringConsts.TEST_DB);

            using (var context = new PBWDbContext(options))
            {
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                //Act && Assert
                Assert.ThrowsException<ArgumentNullException>(() => unitOfWork.UserService.SetUserInitialDetails(null));
            }
        }
    }
}
