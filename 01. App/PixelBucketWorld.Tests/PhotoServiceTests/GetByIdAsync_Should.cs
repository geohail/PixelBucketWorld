﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class GetByIdAsync_Should
    {
        [TestMethod]
        public async Task GetByIdAsync_Should_Throw_IfNotFound()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                    () => unitOfWork.PhotoService.GetByIdAsync(9999, false));
            }
        }

        [TestMethod]
        public async Task GetByIdAsync_Should_Return_Correctly()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Id = 999,
                    Title = "Photo999",
                    Story = "Photo 999 story.",
                    PhotoUrl = "/images/UploadedPhotos/pic999.jpg",
                    UserId = 6,
                    ContestId = 5,
                    ContestRanking = null,

                };

                context.Photos.Add(newPhoto);
                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                var photo = await unitOfWork.PhotoService.GetByIdAsync(999, false);

                // Act & Assert
                Assert.AreEqual(999, photo.Id);
            }
        }

    }
}
