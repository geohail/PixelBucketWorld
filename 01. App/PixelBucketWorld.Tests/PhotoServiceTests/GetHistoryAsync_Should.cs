﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class GetHistoryAsync_Should
    {
        [DataTestMethod]
        public async Task GetByIdAsync_Should_ReturnCorrectCount()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var photos = await unitOfWork
                    .PhotoService.GetHistoryAsync();

                // Assert
                Assert.AreEqual(10, photos.Count());
            }

        }

    }
}
