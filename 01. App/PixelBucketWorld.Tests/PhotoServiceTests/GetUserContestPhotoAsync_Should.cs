﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class GetUserContestPhotoAsync_Should
    {
        [TestMethod]
        [DataRow(3, 2, 5)]
        [DataRow(2, 2, 4)]
        public async Task GetUserContestPhotoAsync_Should_Return_Correctly
            (int contestId, int userId, int expectedPhotoId)
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var photo = await unitOfWork.PhotoService
                    .GetUserContestPhotoAsync(contestId, userId);

                // Assert
                Assert.AreEqual(expectedPhotoId, photo.Id);
            }
        }
    }
}
