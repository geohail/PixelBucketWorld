﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfContestNotCorrect()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 2,
                    ContestId = 999,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                    () => unitOfWork.PhotoService.CreateAsync(newPhoto));
            }
        }

        [TestMethod]
        [DataRow(101)]
        [DataRow(4)]
        public async Task CreateAsync_Should_Throw_IfPhaseIncorrect
            (int contestId)
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 101,
                    ContestId = contestId,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoService.CreateAsync(newPhoto));
            }
        }

        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfParticipantIsJuror()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 5,
                    ContestId = 3,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoService.CreateAsync(newPhoto));
            }
        }


        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfParticipantNotInvited()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 6,
                    ContestId = 2,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoService.CreateAsync(newPhoto));
            }
        }

        [TestMethod]
        public async Task CreateAsync_Should_Throw_IfParticipantNotAccepted()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newInvitation = new InvitationalMessage()
                {
                    Id = 999,
                    Title = "Contest participation invitation!",
                    Content = $"Congratulations ...! You have been invited to participate in the new contest ...",
                    ReceiverId = 6,
                    SenderId = 1,
                    ContestId = 2,
                    InvitationDate = DateTime.Now.AddDays(-2),
                    ContestAccepted = false,
                    HasBeenViewed = false
                };

                context.InvitationalMessages.Add(newInvitation);
                context.SaveChanges();

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 6,
                    ContestId = 2,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoService.CreateAsync(newPhoto));
            }
        }

        [TestMethod]
        public async Task CreateAsync_Should_Throw_AlreadyUploaded()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 2,
                    ContestId = 2,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<InvalidOperationException>(
                    () => unitOfWork.PhotoService.CreateAsync(newPhoto));
            }
        }

        [DataTestMethod]
        public async Task CreateAsync_Should_AddToContext_IfValidModel()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 6,
                    ContestId = 1,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act
                await unitOfWork.PhotoService.CreateAsync(newPhoto);

                var state = context.Entry(newPhoto).State;

                // Assert
                Assert.IsTrue(state == Microsoft.EntityFrameworkCore.EntityState.Added);
            }

        }

        [DataTestMethod]
        public async Task CreateAsync_Should_ReturnTheCreatedObject_IfValidModel()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var newPhoto = new Photo()
                {
                    Title = "new photo title",
                    Story = "new photo story",
                    PhotoUrl = "/images/UploadedPhotos/newPhoto.jpg",
                    UserId = 6,
                    ContestId = 1,
                    ContestRanking = null,

                };

                var unitOfWork = new UnitOfWork(context);

                // Act
                Photo returnedPhoto = await unitOfWork.PhotoService.CreateAsync(newPhoto);

                // Assert
                Assert.AreEqual(newPhoto.Title, returnedPhoto.Title);
                Assert.AreEqual(newPhoto.PhotoUrl, returnedPhoto.PhotoUrl);
            }

        }

    }
}
