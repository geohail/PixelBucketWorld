﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class DoesCoverPhotoExistsLocally_Should
    {
        [TestMethod]
        [DataRow("EXISTING_URL", true)]
        [DataRow("NOT_EXISTING_URL", false)]
        public async Task DoesCoverPhotoExistsLocally_Should_ReturnCorrect(string photoUrl, bool expectedResult)
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var result = await unitOfWork
                    .PhotoService.DoesCoverPhotoExistsLocally(photoUrl);

                // Assert
                Assert.AreEqual(expectedResult, result);
            }

        }

    }
}
