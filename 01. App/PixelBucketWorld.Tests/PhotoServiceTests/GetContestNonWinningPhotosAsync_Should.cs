﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.PhotoServiceTests
{
    [TestClass]
    public class GetContestNonWinningPhotosAsync_Should
    {
        [TestMethod]
        [DataRow(101, 0)]
        public async Task GetContestNonWinningPhotosAsync_Should_Return_Correctly
            (int contestId, int expectedCount)
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var photos = await unitOfWork.PhotoService
                    .GetContestNonWinningPhotosAsync(contestId);

                // Assert
                Assert.AreEqual(expectedCount, photos.Count());
            }
        }

    }
}
