﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class GetParticipantOnGoingContestAsync_Should
    {
        [DataTestMethod]
        [DataRow(1, 0)]
        [DataRow(2, 3)]
        [DataRow(3, 3)]
        public async Task GetParticipantOnGoingContestAsync_Should_ReturnCorrectCount
            (int userId, int expectedCount)
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork
                    .ContestService.GetParticipantOnGoingContestAsync(userId);

                // Assert
                Assert.AreEqual(expectedCount, contests.Count());
            }

        }

    }
}
