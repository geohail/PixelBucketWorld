﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class GetByIdAsync_Should
    {
        [DataTestMethod]
        public async Task GetByIdAsync_Should_Throw_IfNotFound()
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(
                    () => unitOfWork.ContestService.GetByIdAsync(9999));
            }

        }

        [TestMethod]
        public async Task GetByIdAsync_Should_Should_ReturnCorrectEntity()
        {
            // Arrange
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork
                    .ContestService.GetByIdAsync(101);

                // Assert
                Assert.AreEqual("Small Boats", contests.Name);
            }

        }

    }
}
