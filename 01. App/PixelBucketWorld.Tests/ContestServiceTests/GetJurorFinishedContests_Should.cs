﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class GetJurorFinishedContests_Should
    {
        [DataTestMethod]
        [DataRow(1, 0)]
        [DataRow(6, 1)]
        [DataRow(5, 2)]
        public async Task GetJurorFinishedContests_Should_ReturnCorrectCount
            (int userId, int expectedCount)
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                // add a user 6 as juror to contest 101
                // add a user 5 as juror to contest 102, 103

                var newJurorLinks = new List<ContestJurorLink>()
                {
                    new ContestJurorLink
                    {
                        UserId = 6,
                        ContestId = 101,
                        IsAccepted = true
                    },
                    new ContestJurorLink
                    {
                        UserId = 5,
                        ContestId = 102,
                        IsAccepted = true
                    },
                    new ContestJurorLink
                    {
                        UserId = 5,
                        ContestId = 103,
                        IsAccepted = true
                    },
                };

                context.ContestJurorLinks.AddRange(newJurorLinks);
                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork
                    .ContestService.GetJurorFinishedContests(userId);

                // Assert
                Assert.AreEqual(expectedCount, contests.Count());
            }

        }

    }
}
