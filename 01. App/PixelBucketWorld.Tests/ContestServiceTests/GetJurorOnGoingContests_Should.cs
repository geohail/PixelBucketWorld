﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class GetJurorOnGoingContests_Should
    {
        [DataTestMethod]
        [DataRow(1, 0)]
        [DataRow(4, 0)]
        [DataRow(5, 1)]
        public async Task GetJurorOnGoingContests_Should_ReturnCorrectCount
            (int userId, int expectedCount)
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork
                    .ContestService.GetJurorOnGoingContests(userId);

                // Assert
                Assert.AreEqual(expectedCount, contests.Count());
            }

        }

    }
}
