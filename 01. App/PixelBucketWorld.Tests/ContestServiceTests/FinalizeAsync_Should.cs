﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class FinalizeAsync_Should
    {
        [TestMethod]
        public async Task FinalizeAsync_Should_RateCorrectly_OnePhoto()
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Arrange

                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);
                var contest = context.Contests.Find(5);
                var photo = context.Photos.Find(10);

                // Act
                await unitOfWork.ContestService.FinalizeAsync(new List<Contest>() { contest });
                await unitOfWork.SaveAsync();

                // Assert
                Assert.AreEqual(1, photo.ContestRanking);

            }

        }

        [TestMethod]
        public async Task FinalizeAsync_Should_Add_DefaultScore_If_NotReviewed()
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                //Util.SeedDatabase(context);

                context.Database.EnsureDeleted();

                context.Users.AddRange(Util.SeedUsers());
                context.Contests.AddRange(Util.SeedContests());
                context.Ranks.AddRange(Util.SeedRanks());

                #region Seed more data - photo with no assesment
                var oneMoreUser = new User()
                {
                    Id = 999,
                    RankId = 1,
                    CurrentRankingPoints = 0,
                    UserName = "oneMoreUser",
                    Email = "oneMoreUser@gmail.com",
                    FirstName = "One",
                    LastName = "User",
                };

                context.Users.Add(oneMoreUser);

                var oneMorePhoto = new Photo()
                {
                    Id = 999,
                    Title = "Photo 999 title",
                    Story = "Photo 999 story ",
                    PhotoUrl = "",
                    UserId = 999,
                    ContestId = 3,
                    ContestRanking = null,

                };

                context.Photos.Add(oneMorePhoto);

                var oneMoreParticipantLink = new ContestParticipantLink()
                {
                    UserId = 999,
                    ContestId = 3,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now
                };

                context.ContestParticipantLinks.Add(oneMoreParticipantLink);

                #endregion

                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                var contest = await context.Contests.FindAsync(3);

                // Act
                await unitOfWork.ContestService.FinalizeAsync(
                    new List<Contest> { contest });

                // Assert
                Assert.AreEqual(3, oneMorePhoto.FinalPoints);

            }
        }

        [TestMethod]
        public async Task FinalizeAsync_Should_Add_Nothing_If_Reviewed()
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                //Util.SeedDatabase(context);

                context.Database.EnsureDeleted();

                context.Users.AddRange(Util.SeedUsers());
                context.Contests.AddRange(Util.SeedContests());
                context.Ranks.AddRange(Util.SeedRanks());

                #region Seed more data - photo with no assesment
                var oneMoreUser = new User()
                {
                    Id = 999,
                    RankId = 1,
                    CurrentRankingPoints = 0,
                    UserName = "oneMoreUser",
                    Email = "oneMoreUser@gmail.com",
                    FirstName = "One",
                    LastName = "User",
                };

                context.Users.Add(oneMoreUser);

                var oneMorePhoto = new Photo()
                {
                    Id = 999,
                    Title = "Photo 999 title",
                    Story = "Photo 999 story ",
                    PhotoUrl = "",
                    UserId = 999,
                    ContestId = 3,
                    ContestRanking = null,

                };

                context.Photos.Add(oneMorePhoto);

                var oneMoreParticipantLink = new ContestParticipantLink()
                {
                    UserId = 999,
                    ContestId = 3,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now
                };

                context.ContestParticipantLinks.Add(oneMoreParticipantLink);

                var oneMoreAssesment = new PhotoAssessment()
                {
                    Id = 1,
                    Score = 10,
                    Comment = "Best photo!",
                    UserId = 1,
                    PhotoId = 999,
                };

                context.PhotoAssessments.Add(oneMoreAssesment);

                #endregion

                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                var contest = await context.Contests.FindAsync(3);

                // Act
                await unitOfWork.ContestService.FinalizeAsync(
                    new List<Contest> { contest });

                var finalPoints = (await context.Photos.FindAsync(999)).FinalPoints;

                // Assert
                Assert.AreEqual(10, finalPoints);

            }
        }

        [TestMethod]
        public async Task FinalizeAsync_Should_CalculateScore_Correctly()
        // conets 3. 1 + 2 invited jurors (one accepted). 1 new photo. assessed by one juror
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {

                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                #region Seed new contest participant. 1 assesment

                var oneMoreUser = new User()
                {
                    Id = 999,
                    RankId = 1,
                    CurrentRankingPoints = 0,
                    UserName = "oneMoreUser",
                    Email = "oneMoreUser@gmail.com",
                    FirstName = "One",
                    LastName = "User",
                };

                context.Users.Add(oneMoreUser);

                var oneMorePhoto = new Photo()
                {
                    Id = 999,
                    Title = "Photo 999 title",
                    Story = "Photo 999 story ",
                    PhotoUrl = "",
                    UserId = 999,
                    ContestId = 3,
                    ContestRanking = null,

                };

                context.Photos.Add(oneMorePhoto);

                var oneMoreParticipantLink = new ContestParticipantLink()
                {
                    UserId = 999,
                    ContestId = 3,
                    IsAccepted = false,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now
                };

                context.ContestParticipantLinks.Add(oneMoreParticipantLink);

                var oneMoreAssesment = new PhotoAssessment()
                {
                    Id = 999,
                    Score = 10,
                    Comment = "Best photo!",
                    UserId = 1,
                    PhotoId = 999,
                };

                context.PhotoAssessments.Add(oneMoreAssesment);

                #endregion

                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                var contest = await context.Contests.FindAsync(3);

                // Act
                await unitOfWork.ContestService.FinalizeAsync(
                    new List<Contest> { contest });

                var finalPoints = (await context.Photos.FindAsync(999)).FinalPoints;

                // Assert
                // exoected (10 + 3) / 2 = 6.5
                Assert.AreEqual(6.5, finalPoints);

            }
        }

        [TestMethod]
        public async Task FinalizeAsync_Should_AssignUsersPoints_Correctly_NoDoubleScore()
        // Furst / Second place score < 2
        // conets 3. 1 + 2 invited jurors (one accepted).
        // 4 new photos. assessed by one juror
        // finaly have 8 photos every 2 of them have equal score
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {

                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                #region Seed new contest participant. 1 assesment

                for (int i = 1; i <= 4; i++)
                {
                    var oneMoreUser = new User()
                    {
                        Id = 900 + i,
                        RankId = 1,
                        CurrentRankingPoints = 0,
                        UserName = $"oneMoreUser {i}",
                        Email = $"oneMoreUser{i}@gmail.com",
                        FirstName = "One",
                        LastName = $"More User {i}",
                    };

                    context.Users.Add(oneMoreUser);
                }

                for (int i = 1; i <= 4; i++)
                {
                    var oneMorePhoto = new Photo()
                    {
                        Id = 900 + i,
                        Title = $"Photo {900 + i} title",
                        Story = $"Photo {900 + i} story ",
                        PhotoUrl = "",
                        UserId = 900 + i,
                        ContestId = 3,
                        ContestRanking = null,

                    };

                    context.Photos.Add(oneMorePhoto);
                }


                for (int i = 1; i <= 4; i++)
                {
                    var oneMoreParticipantLink = new ContestParticipantLink()
                    {
                        UserId = 900 + i,
                        ContestId = 3,
                        IsAccepted = false,
                        ParticipationDate = null,
                        UploadingPhotoTime = DateTime.Now
                    };

                    context.ContestParticipantLinks.Add(oneMoreParticipantLink);

                }

                for (int i = 1; i <= 4; i++)
                {
                    var oneMoreAssesment = new PhotoAssessment()
                    {
                        Id = 900 + i,
                        Score = 11 - i,
                        Comment = "comment",
                        UserId = 900 + i,
                        PhotoId = 900 + i,
                    };

                    context.PhotoAssessments.Add(oneMoreAssesment);
                }

                #endregion

                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                var contest = await context.Contests.FindAsync(3);

                // Act
                await unitOfWork.ContestService.FinalizeAsync(
                    new List<Contest> { contest });

                var CurrentRankingPoints_901 = (await context.Users.FindAsync(901)).CurrentRankingPoints;
                var CurrentRankingPoints_902 = (await context.Users.FindAsync(902)).CurrentRankingPoints;
                var CurrentRankingPoints_903 = (await context.Users.FindAsync(903)).CurrentRankingPoints;
                var CurrentRankingPoints_904 = (await context.Users.FindAsync(904)).CurrentRankingPoints;

                // Assert
                Assert.AreEqual(41, CurrentRankingPoints_901);
                Assert.AreEqual(26, CurrentRankingPoints_902);
                Assert.AreEqual(11, CurrentRankingPoints_903);
                Assert.AreEqual(1, CurrentRankingPoints_904);

            }
        }

        [TestMethod]
        public async Task FinalizeAsync_Should_AssignUsersPoints_Correctly_DoubleScore()
        // Furst / Second place score >= 2
        // test contest
        // 4 photos, 1 with score 10. assessed by one juror
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {

                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                #region Seed new contest participant. 1 assesment

                var testContest = new Contest()
                {
                    Id = 999,
                    CategoryId = 1,
                    Name = "Contest 999",
                    PhaseId = 5,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-4),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-3),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-2),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-1)
                };
                context.Contests.Add(testContest);

                for (int i = 1; i <= 4; i++)
                {
                    var oneMoreUser = new User()
                    {
                        Id = 900 + i,
                        RankId = 1,
                        CurrentRankingPoints = 0,
                        UserName = $"oneMoreUser {i}",
                        Email = $"oneMoreUser{i}@gmail.com",
                        FirstName = "One",
                        LastName = $"More User {i}",
                    };

                    context.Users.Add(oneMoreUser);
                }

                for (int i = 1; i <= 4; i++)
                {
                    var oneMorePhoto = new Photo()
                    {
                        Id = 900 + i,
                        Title = $"Photo {900 + i} title",
                        Story = $"Photo {900 + i} story ",
                        PhotoUrl = "",
                        UserId = 900 + i,
                        ContestId = 999,
                        ContestRanking = null,

                    };

                    context.Photos.Add(oneMorePhoto);
                }

                for (int i = 1; i <= 4; i++)
                {
                    var oneMoreParticipantLink = new ContestParticipantLink()
                    {
                        UserId = 900 + i,
                        ContestId = 3,
                        IsAccepted = false,
                        ParticipationDate = null,
                        UploadingPhotoTime = DateTime.Now
                    };

                    context.ContestParticipantLinks.Add(oneMoreParticipantLink);

                }

                var firstPlaceAssesment = new PhotoAssessment()
                {
                    Id = 901,
                    Score = 10,
                    Comment = "comment",
                    UserId = 901,
                    PhotoId = 901
                };

                context.PhotoAssessments.Add(firstPlaceAssesment);

                for (int i = 2; i <= 4; i++)
                {
                    var assesment = new PhotoAssessment()
                    {
                        Id = 900 + i,
                        Score = 5 - i,
                        Comment = "comment",
                        UserId = 900 + i,
                        PhotoId = 900 + i,
                    };

                    context.PhotoAssessments.Add(assesment);
                }

                #endregion

                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                var contest = await context.Contests.FindAsync(testContest.Id);

                // Act
                await unitOfWork.ContestService.FinalizeAsync(
                    new List<Contest> { contest });

                var CurrentRankingPoints_901 = (await context.Users.FindAsync(901)).CurrentRankingPoints;
                var CurrentRankingPoints_902 = (await context.Users.FindAsync(902)).CurrentRankingPoints;
                var CurrentRankingPoints_903 = (await context.Users.FindAsync(903)).CurrentRankingPoints;
                var CurrentRankingPoints_904 = (await context.Users.FindAsync(904)).CurrentRankingPoints;

                // Assert
                Assert.AreEqual(76, CurrentRankingPoints_901);
                Assert.AreEqual(36, CurrentRankingPoints_902);
                Assert.AreEqual(21, CurrentRankingPoints_903);
                Assert.AreEqual(1, CurrentRankingPoints_904);

            }
        }

        [TestMethod]
        public async Task FinalizeAsync_Should_ChangeRank_Correctly()
        // Furst / Second place score >= 2
        // test contest
        // 4 photos. assessed by one juror
        {
            // Arrange

            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {

                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                #region Seed new contest participant. 1 assesment

                var testContest = new Contest()
                {
                    Id = 999,
                    CategoryId = 1,
                    Name = "Contest 999",
                    PhaseId = 5,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-4),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-3),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-2),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-1)
                };
                context.Contests.Add(testContest);

                for (int i = 1; i <= 4; i++)
                {
                    var oneMoreUser = new User()
                    {
                        Id = 900 + i,
                        RankId = 2,
                        CurrentRankingPoints = 100,
                        UserName = $"oneMoreUser {i}",
                        Email = $"oneMoreUser{i}@gmail.com",
                        FirstName = "One",
                        LastName = $"More User {i}",
                    };

                    context.Users.Add(oneMoreUser);
                }

                for (int i = 1; i <= 4; i++)
                {
                    var oneMorePhoto = new Photo()
                    {
                        Id = 900 + i,
                        Title = $"Photo {900 + i} title",
                        Story = $"Photo {900 + i} story ",
                        PhotoUrl = "",
                        UserId = 900 + i,
                        ContestId = 999,
                        ContestRanking = null,

                    };

                    context.Photos.Add(oneMorePhoto);
                }

                for (int i = 1; i <= 4; i++)
                {
                    var oneMoreParticipantLink = new ContestParticipantLink()
                    {
                        UserId = 900 + i,
                        ContestId = 3,
                        IsAccepted = false,
                        ParticipationDate = null,
                        UploadingPhotoTime = DateTime.Now
                    };

                    context.ContestParticipantLinks.Add(oneMoreParticipantLink);

                }

                for (int i = 1; i <= 4; i++)
                {
                    var assesment = new PhotoAssessment()
                    {
                        Id = 900 + i,
                        Score = 5 - i,
                        Comment = "comment",
                        UserId = 900 + i,
                        PhotoId = 900 + i,
                    };

                    context.PhotoAssessments.Add(assesment);
                }

                #endregion

                context.SaveChanges();

                var unitOfWork = new UnitOfWork(context);

                var contest = await context.Contests.FindAsync(testContest.Id);

                // Act
                await unitOfWork.ContestService.FinalizeAsync(
                    new List<Contest> { contest });

                var userRank1 = (await context.Users.FindAsync(901)).RankId;
                var userRank2 = (await context.Users.FindAsync(902)).RankId;
                var userRank3 = (await context.Users.FindAsync(903)).RankId;
                var userRank4 = (await context.Users.FindAsync(904)).RankId;

                // Assert
                Assert.AreEqual(3, userRank1);
                Assert.AreEqual(2, userRank2);
                Assert.AreEqual(2, userRank3);
                Assert.AreEqual(2, userRank4);

            }
        }


    }
}
