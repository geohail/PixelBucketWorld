﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PixeBucketWorld.Data;
using PixelBucketWorld.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Tests.ContestServiceTests
{
    [TestClass]
    public class GetParticipantContestsAsync_Should
    {
        [DataTestMethod]
        [DataRow(1, null, 0)]
        [DataRow(2, null, 6)]
        [DataRow(2, 5, 3)]
        [DataRow(6, null, 5)]
        [DataRow(6, 5, 4)]
        public async Task GetParticipantContestsAsync_Should_ReturnCorrectCount
            (int userId, int? phaseId, int expectedCount)
        {
            var options = Util.GetDbContextOptions("TestDb");

            using (var context = new PBWDbContext(options))
            {
                // Seed and save the database
                Util.SeedDatabase(context);

                var unitOfWork = new UnitOfWork(context);

                // Act
                var contests = await unitOfWork
                    .ContestService.GetParticipantContestsAsync(userId, phaseId);

                // Assert
                Assert.AreEqual(expectedCount, contests.Count());
            }

        }
    }
}
