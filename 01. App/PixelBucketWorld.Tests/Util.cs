﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PixeBucketWorld.Data;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Strings;
using System;
using System.Collections.Generic;
using System.Text;

namespace PixelBucketWorld.Tests
{
    public static class Util
    {
        public static DbContextOptions GetDbContextOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<PBWDbContext>()
                .UseInMemoryDatabase(databaseName).Options;
        }

        internal static void SeedDatabase(PBWDbContext context)
        {
            context.Database.EnsureDeleted();

            context.AddRange(SeedRanks());
            context.AddRange(SeedContetsTypes());
            context.AddRange(SeedPhases());
            context.AddRange(SeedUsers());
            context.AddRange(SeedRoles());
            context.AddRange(SeedUserRoles());
            context.AddRange(SeedCategoies());
            context.AddRange(SeedContests());
            context.AddRange(SeedPhoto());
            context.AddRange(SeedPhotoAssesments());
            context.AddRange(SeedMessages());
            context.AddRange(SeedContestParticipantLink());
            context.AddRange(SeedContestJurorLink());

            context.SaveChanges();
        }

        // Fixed data
        internal static ICollection<Rank> SeedRanks()
        {
            return new List<Rank>()
            {
                new Rank() { Id = 1, Name = "Junkie", RequiredPoints = 0},
                new Rank() { Id = 2, Name = "Enthusiast", RequiredPoints = 51},
                new Rank() { Id = 3, Name = "Master", RequiredPoints = 151 },
                new Rank() { Id = 4, Name = "Wise and Benevolent Photo Dictator", RequiredPoints = 1001}
            };
        }

        internal static ICollection<Role> SeedRoles()
        {
            var roles = new List<Role>()
            {
                new Role()
                {
                    Id = 1,
                    Name = UserRoleConsts.ORGANISER,
                    NormalizedName = "ORGANISER"
                },

                new Role()
                {
                    Id = 2,
                    Name = UserRoleConsts.PARTICIPANT,
                    NormalizedName = "PARTICIPANT"
                }
            };

            return roles;
        }

        internal static ICollection<IdentityUserRole<int>> SeedUserRoles()
        {
            var userRoles = new List<IdentityUserRole<int>>()
            {
                 new IdentityUserRole<int>() { RoleId = 1, UserId = 1 },
                 new IdentityUserRole<int>() { RoleId = 2, UserId = 2 },
                 new IdentityUserRole<int>() { RoleId = 2, UserId = 3 },
                 new IdentityUserRole<int>() { RoleId = 2, UserId = 4 },
                 new IdentityUserRole<int>() { RoleId = 2, UserId = 5 },
                 new IdentityUserRole<int>() { RoleId = 2, UserId = 6 },
            };

            return userRoles;
        }

        internal static ICollection<ContestType> SeedContetsTypes()
        {
            return new List<ContestType>()
            {
                new ContestType() { Id = 1, Name = "Open" },
                new ContestType() { Id = 2, Name = "Invitational" }
            };
        }

        internal static ICollection<Phase> SeedPhases()
        {
            return new List<Phase>()
            {
                new Phase() { Id = 1, Name = "Prestart" },
                new Phase() { Id = 2, Name = "Phase I" },
                new Phase() { Id = 3, Name = "Transition" },
                new Phase() { Id = 4, Name = "Phase II" },
                new Phase() { Id = 5, Name = "Finished" }
            };
        }

        internal static ICollection<User> SeedOrganizer()
        {
            return new List<User>()
            {
                // Organiser
                new User() {
                    Id=1,
                    RankId = null,
                    CurrentRankingPoints = null,
                    UserName = "mihailpopov",
                    Email = "mihailpopov@abv.bg",
                    FirstName = "Mihail",
                    LastName = "Popov",
                    },
            };
        }

        // Variable data
        internal static ICollection<User> SeedUsers()
        {
            return new List<User>()
            {
                // Organiser
                new User() {
                    Id=1,
                    RankId = null,
                    CurrentRankingPoints = null,
                    UserName = "mihailpopov",
                    Email = "mihailpopov@abv.bg",
                    FirstName = "Mihail",
                    LastName = "Popov",
                    },

                // Junkie ... to be participant
                new User() {
                    Id = 2,
                    RankId = 1,
                    CurrentRankingPoints = 0,
                    UserName = "georgistefanov",
                    Email = "georgistefanov@abv.bg",
                    FirstName = "Georgi",
                    LastName = "Stefanov",
                    },

                // Enthusiast ... to be participant
                new User() {
                    Id = 3,
                    RankId = 2,
                    CurrentRankingPoints = 60,
                    UserName = "bengunn",
                    Email = "bengunn@gmail.com",
                    FirstName = "Ben",
                    LastName = "Gunn",
                    },

                // Master ... to be invited partcipant
                new User() {
                    Id = 4,
                    RankId = 3,
                    CurrentRankingPoints = 900,
                    UserName = "longjohnsilver",
                    Email = "johnsilver@gmail.com",
                    FirstName = "John",
                    LastName = "Silver",
                    },

                // Dictator ... to be invited jurer
                new User() {
                    Id = 5,
                    RankId = 4,
                    CurrentRankingPoints = 2000,
                    UserName = "jimhawkins",
                    Email = "treasure@gmail.com",
                    FirstName = "Jim",
                    LastName = "Hawkins",
                    },

                // Junkie ... to be participant
                new User() {
                    Id = 6,
                    RankId = 1,
                    CurrentRankingPoints = 45,
                    UserName = "CaptainFlint",
                    Email = "flint@gmail.com",
                    FirstName = "Captain",
                    LastName = "Flint",
                    },
            };
        }

        internal static ICollection<Category> SeedCategoies()
        {
            return new List<Category>()
            {
                new Category() { Id = 1, Name = "Animals"},
                new Category() { Id = 2, Name = "Nature" },
                new Category() { Id = 3, Name = "Boats" },
                new Category() { Id = 4, Name = "Bridges" },
                new Category() { Id = 5, Name = "Faces" },
            };

        }

        internal static ICollection<Contest> SeedContests()
        {
            return new List<Contest>()
            {
                // Open Finished
                new Contest() {
                    Id = 101,
                    CategoryId = 3,
                    Name = "Small Boats",
                    PhaseId = 5,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-10),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-9),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-8),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-7)},

                new Contest() {
                    Id = 102,
                    CategoryId = 4,
                    Name = "Long Bridges",
                    PhaseId = 5,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-10),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-9),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-8),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-7)},

                new Contest() {
                    Id = 103,
                    CategoryId = 5,
                    Name = "One Human Face",
                    PhaseId = 5,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-10),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-9),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-8),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-7)},

                // Active
                // Contest 1, Phase I, open
                new Contest() {
                    Id = 1,
                    CategoryId = 1,
                    Name = "Cuttest Animals",
                    PhaseId = 2,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-1),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(1),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(3),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(4),
                    PhotoUrl = "/images/ContestPhotos/Anymals.jpg"},

                // Contest 2, Phase I, invitational, jury could be invited
                new Contest() {
                    Id = 2,
                    CategoryId = 1,
                    Name = "Animals in the wild",
                    PhaseId = 2,
                    TypeId = 2,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-2),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(2),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(3),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(4),
                    PhotoUrl = "/images/ContestPhotos/Anymals.jpg"},

                // Contest 3, Phase II, open, jury is invited
                new Contest() {
                    Id = 3,
                    CategoryId = 2,
                    Name = "Motorcicle touring",
                    PhaseId = 4,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-4),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-3),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-2),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-1),
                    PhotoUrl = "/images/ContestPhotos/Nature.jpg"},

                 // Contest 4, Phase Prestart, invitational
                new Contest() {
                    Id = 4,
                    CategoryId = 1,
                    Name = "Animals in the wild - 2",
                    PhaseId = 1,
                    TypeId = 2,
                    PhaseOneStartingDate = DateTime.Now.AddDays(2),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(3),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(4),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(5),
                    PhotoUrl = "/images/ContestPhotos/PixelBucketWorld.jpg"},

                // open, finished, one photo
                new Contest() {
                    Id = 5,
                    CategoryId = 1,
                    Name = "Finished contest with one photo",
                    PhaseId = 5,
                    TypeId = 1,
                    PhaseOneStartingDate = DateTime.Now.AddDays(-4),
                    PhaseOneFinishingDate = DateTime.Now.AddDays(-3),
                    PhaseTwoStartingDate = DateTime.Now.AddDays(-2),
                    PhaseTwoFinishingDate = DateTime.Now.AddDays(-1),
                    PhotoUrl = "EXISTING_URL"},

            };
        }

        internal static ICollection<Photo> SeedPhoto()
        {
            return new List<Photo>()
            {
#region Finished contests
		                // Finished contests
                new Photo() {
                Id = 101,
                Title = "A boat",
                Story = "A cleverly crafted phrase like Black Pearl or Bankers Hours makes a statement without having to say to much.",
                PhotoUrl = "/images/UploadedPhotos/pic101.1.jpg",
                UserId = 2,
                ContestId = 101,
                ContestRanking = 1,
                FinalPoints = 10
                },
                new Photo() {
                Id = 102,
                Title = "Another boat",
                Story = "Whether you’ve hopped on board a super-yacht or are sailing around on a sunfish– there’s nothing better than hitting the high seas.",
                PhotoUrl = "/images/UploadedPhotos/pic101.2.jpg",
                UserId = 3,
                ContestId = 101,
                ContestRanking = 2,
                FinalPoints = 9
                },
                new Photo() {
                Id = 103,
                Title = "And another one boat",
                Story = "From the iconic names of super-yachts to sea-worthy puns, we’ve rounded up the most chic, popular, and all around classic handles to give you the best boat names of 2019.",
                PhotoUrl = "/images/UploadedPhotos/pic101.3.jpg",
                UserId = 6,
                ContestId = 101,
                ContestRanking = 3,
                FinalPoints = 8
                },


                new Photo() {
                Id = 201,
                Title = "A bridge",
                Story = "Free running water under bridges of golden rock, those are the bridges that call us all home in our evening years.",
                PhotoUrl = "/images/UploadedPhotos/pic102.1.jpg",
                UserId = 2,
                ContestId = 102,
                ContestRanking = 1,
                FinalPoints = 10
                },
                new Photo() {
                Id = 202,
                Title = "Another bridge",
                Story = "The bridges are marvels of engineering, yet of pale beauty by comparison to our river.",
                PhotoUrl = "/images/UploadedPhotos/pic102.2.jpg",
                UserId = 3,
                ContestId = 102,
                ContestRanking = 2,
                FinalPoints = 9
                },
                new Photo() {
                Id = 203,
                Title = "And another one bridge",
                Story = "The bridges of the county are hewn from granite in our mountains. They have stood strong for generations, linking the folks in these parts, giving the sweetest of spots for romance and reunions.",
                PhotoUrl = "/images/UploadedPhotos/pic102.3.jpg",
                UserId = 6,
                ContestId = 102,
                ContestRanking = 3,
                FinalPoints = 8
                },


                new Photo() {
                Id = 301,
                Title = "A face",
                Story = "grayscale photography, man, looking, person, human, face, portrait, close, black and white, fold, age, male, adult, senior adult, headshot, one person",
                PhotoUrl = "/images/UploadedPhotos/pic103.1.jpg",
                UserId = 2,
                ContestId = 103,
                ContestRanking = 1,
                FinalPoints = 10
                },
                new Photo() {
                Id = 302,
                Title = "Another face",
                Story = "...so many strange contrasts in one human face...",
                PhotoUrl = "/images/UploadedPhotos/pic103.2.jpg",
                UserId = 3,
                ContestId = 103,
                ContestRanking = 2,
                FinalPoints = 9
                },
                new Photo() {
                Id = 303,
                Title = "And another one face",
                Story = "Human face can be different with strong expressions and emotions. Funny, positive, squinting with one eye man. Facial expression, emotions.",
                PhotoUrl = "/images/UploadedPhotos/pic103.3.jpg",
                UserId = 6,
                ContestId = 103,
                ContestRanking = 3,
                FinalPoints = 8
                },

	#endregion

#region Contest 1 in phase I
		                // Contest 1 in phase I
                new Photo() {
                Id = 1,
                Title = "Two little bears",
                Story = "I just saw them in the forest.",
                PhotoUrl = "/images/UploadedPhotos/pic1.1.jpg",
                UserId = 2,
                ContestId = 1,
                ContestRanking = null,

                },

                new Photo() {
                Id = 2,
                Title = "Pinguin meeting on the ice",
                Story = "Ready for the party ....",
                PhotoUrl = "/images/UploadedPhotos/pic1.2.jpg",
                UserId = 3,
                ContestId = 1,
                ContestRanking = null,

                },

	#endregion

#region Contest 2 in phase I
		                // Contest 2 in phase I
                new Photo() {
                Id = 3,
                Title = "Killer whales",
                Story = "Killer?",
                PhotoUrl = "/images/UploadedPhotos/pic2.1.jpg",
                UserId = 3,
                ContestId = 2,
                ContestRanking = null,

                },

                new Photo() {
                Id = 4,
                Title = "Mama bear",
                Story = "I dropped it in my pants and run away.",
                PhotoUrl = "/images/UploadedPhotos/pic2.2.jpg",
                UserId = 2,
                ContestId = 2,
                ContestRanking = null,

                },


	#endregion

#region Contest 3 in phase II. these have assesments
		                // Contest 3 in phase II. these have assesments
                new Photo() {
                Id = 5,
                Title = "5 Reasons Why You Should Travel With A Motorcycle",
                Story = "The freedom of being on a bike is bliss isn’t it? You have a full 360 degree Panoramic view of your environment and you get to your destination far more relaxed than you would in a car. ",
                PhotoUrl = "/images/UploadedPhotos/pic3.1.jpg",
                UserId = 2,
                ContestId = 3,
                ContestRanking = null,

                },

                new Photo() {
                Id = 6,
                Title = "BIKER’S BUCKET LIST",
                Story = "TOURING NORWAY BY MOTORCYCLE",
                PhotoUrl = "/images/UploadedPhotos/pic3.2.jpg",
                UserId = 3,
                ContestId = 3,
                ContestRanking = null,

                },

                new Photo() {
                Id = 7,
                Title = "Motorcycle Touring Tips",
                Story = "We've put together a complete guide with all the motorcycle touring tips and tricks you need to know.",
                PhotoUrl = "/images/UploadedPhotos/pic3.3.jpg",
                UserId = 4,
                ContestId = 3,
                ContestRanking = null,

                },

                new Photo() {
                Id = 8,
                Title = "Motorcycle Touring in Spain and Portugal",
                Story = "Quite simply, it’s the riding. The Iberian peninsula is huge, with every kind of landscape you could imagine – from verdant Galicia on the north-west Atlantic coast to sun-baked Murcia on the Mediterranean.",
                PhotoUrl = "/images/UploadedPhotos/pic3.4.jpg",
                UserId = 6,
                ContestId = 3,
                ContestRanking = null,

                },

	#endregion


                new Photo() {
                Id = 10,
                Title = "Photo10",
                Story = "Photo 10 story.",
                PhotoUrl = "/images/UploadedPhotos/pic10.jpg",
                UserId = 6,
                ContestId = 5,
                ContestRanking = null,

                },



            };

        }

        internal static ICollection<PhotoAssessment> SeedPhotoAssesments()
        {
            return new List<PhotoAssessment>()
            {
#region Finished contests

		        // Finished contests
                new PhotoAssessment()
                {
                    Id = 101,
                    Score = 10,
                    Comment = "Best photo!",
                    UserId = 1,
                    PhotoId = 101,
                },
                new PhotoAssessment()
                {
                    Id = 102,
                    Score = 9,
                    Comment = "Good photo!",
                    UserId = 1,
                    PhotoId = 102,
                },
                new PhotoAssessment()
                {
                    Id = 103,
                    Score = 8,
                    Comment = "Not a bad photo!",
                    UserId = 1,
                    PhotoId = 103,
                },

                new PhotoAssessment()
                {
                    Id = 201,
                    Score = 10,
                    Comment = "Best photo!",
                    UserId = 1,
                    PhotoId = 201,
                },

                new PhotoAssessment()
                {
                    Id = 202,
                    Score = 9,
                    Comment = "Good photo!",
                    UserId = 1,
                    PhotoId = 202,
                },

                new PhotoAssessment()
                {
                    Id = 203,
                    Score = 8,
                    Comment = "Not a bad photo!",
                    UserId = 1,
                    PhotoId = 203,
                },

                new PhotoAssessment()
                {
                    Id = 301,
                    Score = 10,
                    Comment = "Best photo!",
                    UserId = 1,
                    PhotoId = 301,
                },
                new PhotoAssessment()
                {
                    Id = 302,
                    Score = 9,
                    Comment = "Good photo!",
                    UserId = 1,
                    PhotoId = 302,
                },
                new PhotoAssessment()
                {
                    Id = 303,
                    Score = 8,
                    Comment = "Not a bad photo!",
                    UserId = 1,
                    PhotoId = 303,
                },

	#endregion

#region Contest 3 in phase II

		        new PhotoAssessment()
                {
                    Id = 1,
                    Score = 10,
                    Comment = "Best photo!",
                    UserId = 1,
                    PhotoId = 5,
                },

                new PhotoAssessment()
                {
                    Id = 2,
                    Score = 9,
                    Comment = "Good job!",
                    UserId = 5,
                    PhotoId = 6,
                },

                new PhotoAssessment()
                {
                    Id = 3,
                    Score = 8,
                    Comment = "Interesting!",
                    UserId = 1,
                    PhotoId = 7,
                },

                new PhotoAssessment()
                {
                    Id = 4,
                    Score = 7,
                    Comment = "You need more experience!",
                    UserId = 5,
                    PhotoId = 8,
                },

	#endregion

                new PhotoAssessment()
                {
                    Id = 5,
                    Score = 5,
                    Comment = "Photo 10 comment",
                    UserId = 1,
                    PhotoId = 10,
                },


            };
        }

        internal static ICollection<ContestParticipantLink> SeedContestParticipantLink()
        {
            return new List<ContestParticipantLink>()
            {
#region Participants in Finished contests
		                // Participants in Finished contests
                new ContestParticipantLink()
                {
                    UserId = 2,
                    ContestId = 101,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },
                new ContestParticipantLink()
                {
                    UserId = 3,
                    ContestId = 101,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },
                new ContestParticipantLink()
                {
                    UserId = 6,
                    ContestId = 101,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },

                new ContestParticipantLink()
                {
                    UserId = 2,
                    ContestId = 102,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },
                new ContestParticipantLink()
                {
                    UserId = 3,
                    ContestId = 102,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },
                new ContestParticipantLink()
                {
                    UserId = 6,
                    ContestId = 102,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },

                new ContestParticipantLink()
                {
                    UserId = 2,
                    ContestId = 103,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },
                new ContestParticipantLink()
                {
                    UserId = 3,
                    ContestId = 103,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },
                new ContestParticipantLink()
                {
                    UserId = 6,
                    ContestId = 103,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-10),
                },

	#endregion

#region participants in open contest 1
		                // participants in open contest 1
                new ContestParticipantLink()
                {
                    UserId = 2,
                    ContestId = 1,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now
                },

                new ContestParticipantLink()
                {
                    UserId = 3,
                    ContestId = 1,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now
                },

	#endregion

#region invited in contest 2
		                // invited in contest 2
                new ContestParticipantLink()
                {
                    UserId = 3,
                    ContestId = 2,
                    IsAccepted = true,
                    ParticipationDate = DateTime.Now.AddDays(-1),
                    UploadingPhotoTime = DateTime.Now
                },

                new ContestParticipantLink()
                {
                    UserId = 2,
                    ContestId = 2,
                    IsAccepted = true,
                    ParticipationDate = DateTime.Now.AddDays(-1),
                    UploadingPhotoTime = DateTime.Now
                },

	#endregion

#region participants in contest 3
		                // participants in contest 3
                new ContestParticipantLink()
                {
                    UserId = 2,
                    ContestId = 3,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-3.5)
                },

                new ContestParticipantLink()
                {
                    UserId = 3,
                    ContestId = 3,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-3.5)
                },

                new ContestParticipantLink()
                {
                    UserId = 4,
                    ContestId = 3,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-3.5)
                },

                new ContestParticipantLink()
                {
                    UserId = 6,
                    ContestId = 3,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-3.5)
                },

	#endregion

                new ContestParticipantLink()
                {
                    UserId = 6,
                    ContestId = 5,
                    IsAccepted = null,
                    ParticipationDate = null,
                    UploadingPhotoTime = DateTime.Now.AddDays(-3.5)
                },

            };
        }

        internal static ICollection<ContestJurorLink> SeedContestJurorLink()
        {
            return new List<ContestJurorLink>()
            {
                new ContestJurorLink()
                {
                    UserId = 4,
                    ContestId = 3,
                    IsAccepted = false // did not accepted. participates in the contest
                },

                new ContestJurorLink()
                {
                    UserId = 5,
                    ContestId = 3,
                    IsAccepted = true
                }
            };
        }


        internal static ICollection<InvitationalMessage> SeedMessages()
        {
            return new List<InvitationalMessage>()
            {
                // participants invitations
                new InvitationalMessage()
                {
                    Id = 1,
                    Title = "Contest participation invitation!",
                    Content = $"Congratulations bengunn! You have been invited to participate in the new contest Anymals in the wild",
                    ReceiverId = 3,
                    SenderId = 1,
                    ContestId = 2,
                    InvitationDate = DateTime.Now.AddDays(-2),
                    ContestAccepted = true,
                    HasBeenViewed = true
                },

                new InvitationalMessage()
                {
                    Id = 2,
                    Title = "Contest participation invitation!",
                    Content = $"Congratulations georgistefanov! You have been invited to participate in the new contest Anymals in the wild",
                    ReceiverId = 2,
                    SenderId = 1,
                    ContestId = 2,
                    InvitationDate = DateTime.Now.AddDays(-2),
                    ContestAccepted = true,
                    HasBeenViewed = true
                },

                // jurors invitations
                new InvitationalMessage()
                {
                    Id = 3,
                    Title = "Contest juror invitation!",
                    Content = $"Congratulations longjohnsilver! You have been invited to be a juror for the new contest Motorcicle touring",
                    ReceiverId = 4,
                    SenderId = 1,
                    ContestId = 3,
                    InvitationDate = DateTime.Now.AddDays(-3),
                    ContestAccepted=false,
                    HasBeenViewed = true
                },

                new InvitationalMessage()
                {
                    Id = 4,
                    Title = "Contest juror invitation!",
                    Content = $"Congratulations jimhawkins! You have been invited to be a juror for the new contest Motorcicle touring",
                    ReceiverId = 5,
                    SenderId = 1,
                    ContestId = 3,
                    InvitationDate = DateTime.Now.AddDays(-3),
                    ContestAccepted=true,
                    HasBeenViewed = true
                },

            };
        }

    }
}