﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PhaseSwitcher
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private Timer timer;
        private readonly IServiceProvider serviceProvider;

        // simulates passing of 1 day
        private static int DAY_IN_MINUTES = 1;

        public TimedHostedService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            Console.WriteLine("Timed Hosted Service running.");

            timer = new Timer(DoWork, null,
                TimeSpan.FromMinutes(DAY_IN_MINUTES),
                TimeSpan.FromMinutes(DAY_IN_MINUTES));

            return Task.CompletedTask;
        }

        //private void DoWork(object state)
        private async void DoWork(object state)
        {
            var count = Interlocked.Increment(ref executionCount);

            Console.WriteLine($"Timed Hosted Service is working. Day: {count}", count);

            using (var scope = serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                var loggingService = scope.ServiceProvider.GetRequiredService<ILoggingService>();

                List<Contest> switchedContests = await unitOfWork.ContestService.SwitchPhasesAsync(DateTime.Now.AddDays(count));

                foreach (var contest in switchedContests)
                {
                    string detailedMessage = $"Switch phase to {contest.PhaseId}";
                    await loggingService.LogInformationAsync(InformationLogEvent.UPDATE_ITEM, detailedMessage, contest);
                }

                var finishedContests = switchedContests.Where(c => c.PhaseId == PhaseConsts.FINISHED).ToList();

                await unitOfWork.ContestService.FinalizeAsync(finishedContests);

                await unitOfWork.SaveAsync();
            }
        }

        // called at graceful shutdown
        public Task StopAsync(CancellationToken stoppingToken)
        {
            // should write in logger when stops "gracefully"
            Console.WriteLine("Timed Hosted Service is stopping.");

            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }

    }
}
