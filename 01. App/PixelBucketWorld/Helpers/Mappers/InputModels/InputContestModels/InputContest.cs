﻿using Microsoft.AspNetCore.Http;
using PixeBucketWorld.Data.CustomDataAnnotations;
using PixeBucketWorld.Data.ValidationAttributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.Map.InputModels.InputContestModels
{
    public class InputContestModel
    { 
        [Required, StringLength(50, MinimumLength = 5, ErrorMessage = "{0} length must be between {2} and {1} characters")]
        public string Title { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int TypeId { get; set; }

        [Required, CurrentDate("Date must be in future")]
        public DateTime PhaseOneStartingDate { get; set; }

        [Required]
        [GreaterThan(nameof(PhaseOneStartingDate), "Phase I finishing date must be greater than the starting date")]
        public DateTime PhaseOneFinishingDate { get; set; }

        [Required]
        [GreaterThan(nameof(PhaseOneFinishingDate), "Phase II starting date must be greater than Phase I finishing date")]
        public DateTime PhaseTwoStartingDate { get; set; }

        [Required]
        [GreaterThan(nameof(PhaseTwoStartingDate), "Phase II finishing date must be greater than the starting date")]
        public DateTime PhaseTwoFinishingDate { get; set; }

        public IFormFile File { get; set; }

    }
}
