﻿using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.Helpers.Mappers.InputModels.InputCategoryModels
{
    public class InputCategory
    {
        [Required, StringLength(30, MinimumLength = 3, ErrorMessage = "Category name must be between {1} and {0} characters")]
        public string Name { get; set; }
    }
}
