﻿using PixeBucketWorld.Data.Models;
using System;

namespace PixelBucketWorld.Helpers.Mappers.OutputModels
{
    public class OutputMessageBase
    {
        public OutputMessageBase()
        {

        }
        public OutputMessageBase(InvitationalMessage invMessage)
        {
            this.Id = invMessage.Id;
            this.Title = invMessage.Title;
            this.Content = invMessage.Content;
            this.InvitationDate = invMessage.InvitationDate;

        }

        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime InvitationDate { get; set; }

    }
}
