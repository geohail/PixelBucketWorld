﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Helpers.Mappers.OutputModels;
using System;

namespace PixelBucketWorld.Map.OutputModels
{
    public class OutputContestPartial : OutputContestBase
    {
        public OutputContestPartial()
        {

        }

        public OutputContestPartial(Contest contest)
            : base(contest)
        {
            Type = contest.Type.Name;
            Category = contest.Category.Name;
            PhaseOneStartingDate = contest.PhaseOneStartingDate;
            PhaseOneFinishingDate = contest.PhaseOneFinishingDate;
            PhaseTwoStartingDate = contest.PhaseTwoStartingDate;
            PhaseTwoFinishingDate = contest.PhaseTwoFinishingDate;
            CurrentPhase = contest.CurrentPhase.Name;

        }

        public string Category { get; set; }

        public string Type { get; set; }

        public DateTime PhaseOneStartingDate { get; set; }

        public DateTime PhaseOneFinishingDate { get; set; }

        public DateTime PhaseTwoStartingDate { get; set; }

        public DateTime PhaseTwoFinishingDate { get; set; }

        public string CurrentPhase { get; set; }
    }
}
