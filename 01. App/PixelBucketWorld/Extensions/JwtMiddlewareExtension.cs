﻿using Microsoft.AspNetCore.Builder;
using PixelBucketWorld.Middlewares;

namespace PixelBucketWorld.Extensions
{
    public static class JwtMiddlewareExtension
    {
        public static void UseJwtMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<JwtMiddleware>();
        }
    }
}
