﻿using PixeBucketWorld.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace PixelBucketWorld.ViewModels.PhotoAssessmentVMs
{
    public class AssessmentViewModel
    {
        public AssessmentViewModel(Photo photo)
        {
            this.PhotoUrl = photo.PhotoUrl;
            this.Username = photo.User.UserName;
            this.Score = photo.FinalPoints;
            this.Comments = photo.PhotoAssessments.Select(pa=>pa.Comment);
        }

        public string PhotoUrl { get; set; }

        public string Username { get; set; }

        public double Score { get; set; }

        public IEnumerable<string> Comments { get; set; }
    }
}
