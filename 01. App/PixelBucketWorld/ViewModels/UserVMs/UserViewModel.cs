﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class UserViewModel
    {
        public UserViewModel(User user)
        {
            this.Username = user.UserName;
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
