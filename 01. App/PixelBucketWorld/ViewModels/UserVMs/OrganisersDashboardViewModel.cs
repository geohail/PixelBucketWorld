﻿using PixelBucketWorld.ViewModels.ContestVMs;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.UserVMs
{
    public class OrganisersDashboardViewModel
    {
        public List<ContestCardViewModel> PhasePrestartContests { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> PhaseOneContests { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> PhaseTransitionContests { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> PhaseTwoContests { get; set; } = new List<ContestCardViewModel>();

        public List<ContestCardViewModel> FinishedContests { get; set; } = new List<ContestCardViewModel>();
    }
}
