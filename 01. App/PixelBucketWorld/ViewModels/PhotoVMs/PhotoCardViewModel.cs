﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.ViewModels.Contracts;
using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.ViewModels.PhotoVMs
{
    public class PhotoCardViewModel : IPhotoCardViewModel
    {
        public PhotoCardViewModel(Photo photo)
        {
            this.PhotoId = photo.Id;
            this.Title = photo.Title;
            this.Story = photo.Story;
            this.PhotoUrl = photo.PhotoUrl;
        }

        public string OwnerUsername { get; set; }

        public int PhotoId { get; set; }

        public string Title { get; set; }

        public string Story { get; set; }

        public string PhotoUrl { get; set; }

        public int RaterId { get; set; }

        public int Score { get; set; }

        public string Comment { get; set; }

        [Display(Name = "Irrelevant photo for current category")]
        public bool isFittingCategory { get; set; }
    }
}
