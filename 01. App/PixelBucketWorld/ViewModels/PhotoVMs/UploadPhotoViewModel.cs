﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.ViewModels.PhotoVMs
{
    public class UploadPhotoViewModel
    {
        public int UserId { get; set; }
        public int ContestId { get; set; }

        [MinLength(3, ErrorMessage = "Photo title must be at least {1} characters")]
        [Display(Name = "Photo title")]
        public string PhotoTitle { get; set; }

        [MinLength(8, ErrorMessage = "Photo story must be at least {1} characters")]
        [Display(Name = "Photo story")]
        public string PhotoStory { get; set; }

        [Required(ErrorMessage = "Please attach photo")]
        public IFormFile Photo { get; set; }
    }
}
