﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using PixeBucketWorld.Data.CustomDataAnnotations;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PixelBucketWorld.ViewModels.ContestVMs
{
    public class CreateContestViewModel
    {
        [StringLength(50, MinimumLength = 5, ErrorMessage = "{0} length must be between {2} and {1} characters")]
        public string Title { get; set; }

        [Required, CurrentDate("Date must be in future")] 
        public DateTime PhaseOneStartingDate { get; set; }

        [Required, GreaterThan(nameof(PhaseOneStartingDate), "Phase I ending date must be greater than the starting date")]
        [PhaseOneTimeInterval(30)]
        public DateTime PhaseOneFinishingDate { get; set; }

        [Required, GreaterThan(nameof(PhaseOneFinishingDate), "Phase II starting date must be greater than Phase I ending date")]
        public DateTime PhaseTwoStartingDate { get; set; }

        [Required, GreaterThan(nameof(PhaseTwoStartingDate), "Phase II ending date must be greater than the starting date")]
        [PhaseTwoTimeInterval(24)]
        public DateTime PhaseTwoFinishingDate { get; set; }

        public IFormFile UploadedCoverPhoto { get; set; }

        public IFormFile ExistingCoverPhoto { get; set; }

        public string PhotoUrl { get; set; }

        public int Category { get; set; }

        public List<SelectListItem> Categories { get; set; } = new List<SelectListItem>();

        public int Type { get; set; }

        public List<SelectListItem> Types { get; set; } = new List<SelectListItem>();

        public List<User> Jury { get; set; } = new List<User>();

        public List<User> UsersToInvite { get; set; } = new List<User>();
    }
}
