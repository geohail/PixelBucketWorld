﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.ViewModels.ContestVMs
{
    public class ContestCardViewModel
    {
        public ContestCardViewModel(Contest contest)
        {
            this.Title = contest.Name;
            this.Category = contest.Category.Name;
            this.CurrentPhase = contest.CurrentPhase.Name;
            this.Id = contest.Id;
            this.Type = contest.Type.Name;
            this.PhotoUrl = contest.PhotoUrl;
        }

        public string PhotoUrl { get; set; }

        public int Id { get; set; }

        public string Title { get; set; }

        public string Type { get; set; }

        public string Category { get; set; }

        public string CurrentPhase { get; set; }
    }
}
