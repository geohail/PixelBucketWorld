﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.ViewModels.Base;
using PixelBucketWorld.ViewModels.PhotoAssessmentVMs;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.ContestVMs
{
    public class ContestPhaseFinishedViewModel : ContestPhaseDetails
    {
        public ContestPhaseFinishedViewModel(Contest contest) : base(contest)
        {

        }

        public bool IsUserJuror { get; set; }
        public AssessmentViewModel CurrentUserPhotoAssessment { get; set; }

        public IEnumerable<AssessmentViewModel> FirstPlaceWinnersAssessments { get; set; }

        public IEnumerable<AssessmentViewModel> SecondPlaceWinnersAssessments { get; set; }

        public IEnumerable<AssessmentViewModel> ThirdPlaceWinnersAssessments { get; set; }

        public IEnumerable<AssessmentViewModel> LoosersPhotoAssessments { get; set; }
    }
}
