﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.ViewModels.Base;
using PixelBucketWorld.ViewModels.PhotoVMs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PixelBucketWorld.ViewModels.ContestVMs
{
    public class ContestPhaseOneDetailsViewModel : ContestPhaseDetails
    {
        public ContestPhaseOneDetailsViewModel() : base()
        {

        }
        public ContestPhaseOneDetailsViewModel(Contest contest) : base(contest)
        {
            this.ContestId = contest.Id;
            this.ContestTypeId = contest.TypeId;
            this.CurrentPhaseDiffInMillisec = (contest.PhaseOneFinishingDate - DateTime.Now).TotalMilliseconds;
            this.CurrentNextPhaseDiffInMillsec = (contest.PhaseTwoStartingDate - DateTime.Now).TotalMilliseconds;
            this.Participants = contest.UploadedPhotos.Select(p => p.User).ToList();
            this.Jury = contest.JurorsLink.Select(jl => jl.User).ToList();
            this.Photos = contest.UploadedPhotos.Select(p => new PhotoCardViewModel(p)).ToList();
            this.InviteesLink = contest.ParticipantsLink.Where(pl => pl.IsAccepted != null).ToList();
        }

        public int ContestId { get; set; }

        public int ContestTypeId { get; set; }

        public double CurrentPhaseDiffInMillisec { get; set; }

        public double CurrentNextPhaseDiffInMillsec { get; set; }

        public string CurrentPhase { get; set; }

        public UploadPhotoViewModel UploadPhotoVM { get; set; }

        public List<User> Participants { get; set; } = new List<User>();

        public List<User> Jury { get; set; } = new List<User>();

        public List<PhotoCardViewModel> Photos { get; set; }

        public List<ContestParticipantLink> InviteesLink { get; set; } = new List<ContestParticipantLink>();
    }
}
