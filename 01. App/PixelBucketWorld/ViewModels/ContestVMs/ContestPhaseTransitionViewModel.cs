﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.ViewModels.Base;
using PixelBucketWorld.ViewModels.PhotoVMs;
using System;
using System.Collections.Generic;

namespace PixelBucketWorld.ViewModels.ContestVMs
{
    public class ContestPhaseTransitionViewModel : ContestPhaseDetails
    {
        public ContestPhaseTransitionViewModel(Contest contest)
            : base(contest)
        {
            this.NextPhaseDiffInMillSec = (contest.PhaseTwoStartingDate - DateTime.Now).TotalMilliseconds;
        }

        public double NextPhaseDiffInMillSec { get; set; }
        public List<PhotoCardViewModel> UploadedPhotos { get; set; }
    }
}
