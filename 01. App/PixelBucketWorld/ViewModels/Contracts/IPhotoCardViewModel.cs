﻿namespace PixelBucketWorld.ViewModels.Contracts
{
    public interface IPhotoCardViewModel
    {
        public string PhotoUrl { get; set; }

        public string Story { get; set; }

        public string Title { get; set; }
    }
}
