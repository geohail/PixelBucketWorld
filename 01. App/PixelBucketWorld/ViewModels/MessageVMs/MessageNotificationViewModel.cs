﻿using PixeBucketWorld.Data.Models;

namespace PixelBucketWorld.ViewModels.MessagesVMs
{
    public class MessageNotificationViewModel
    {
        public MessageNotificationViewModel(InvitationalMessage invitationalMessage)
        {
            this.Title = invitationalMessage.Title;
            this.Id = invitationalMessage.Id;
            this.ContestId = invitationalMessage.ContestId;
            this.HasBeenViewed = invitationalMessage.HasBeenViewed;
            this.Date = invitationalMessage.InvitationDate.ToString("MM/dd/yy HH:mm");
        }
        public int Id { get; set; }

        public int ContestId { get; set; }

        public string Title { get; set; }

        public string Date { get; set; }

        //public bool ContestHasBeenAccepted { get; set; }

        public bool HasBeenViewed { get; set; }
    }
}
