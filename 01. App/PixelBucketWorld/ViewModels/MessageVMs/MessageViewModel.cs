﻿using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Consts.Numbers;

namespace PixelBucketWorld.ViewModels.MessagesVMs
{
    public class MessageViewModel
    {
        public MessageViewModel(InvitationalMessage  invitationalMessage)
        {
            this.ContestId = invitationalMessage.ContestId;
            this.Content = invitationalMessage.Content;
            this.Title = invitationalMessage.Title;
            this.SenderEmail = invitationalMessage.Sender.Email;
            this.ReceiverEmail = invitationalMessage.Receiver.Email;
            this.IsContestBeforePhaseII = invitationalMessage.Contest.CurrentPhase.Id < PhaseConsts.TRANSITION;
            this.IsContestBeforePhaseI = invitationalMessage.Contest.CurrentPhase.Id < PhaseConsts.PHASE_ONE;

            if (invitationalMessage.InvitationDate.Hour >= 12)
            {
                this.Date = invitationalMessage.InvitationDate.ToString("MM/dd/yy hh/mm/ss PM");
            }
            else
            {
                this.Date = invitationalMessage.InvitationDate.ToString("MM/dd/yy hh/mm/ss AM");
            }
        }

        public bool IsUserJuror { get; set; }

        public bool IsContestBeforePhaseI { get; set; }

        public bool IsContestBeforePhaseII { get; set; }

        public bool isContestAccepted { get; set; }

        public string ReceiverEmail { get; set; }

        public string SenderEmail { get; set; }

        public int ContestId { get; set; }

        public string Content { get; set; }

        public string Title { get; set; }

        public string Date { get; set; }
    }
}
