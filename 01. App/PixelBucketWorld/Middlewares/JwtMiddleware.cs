﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PixeBucketWorld.Data.Models;
using PixelBucketWorld.Services.Helpers;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PixelBucketWorld.Middlewares
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate next;

        public JwtMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, IOptions<AppSettings> appSettings, UserManager<User> userManager)
        {
            var token = context.Request.Headers["Authorization"].ToString().Split(' ').Last();

            if (!string.IsNullOrEmpty(token))
            {
                await AttachUserToContext(context, token, appSettings.Value, userManager);
            }

            await this.next(context);
        }

        private async Task AttachUserToContext(HttpContext context, string token, AppSettings appSettings, UserManager<User> userManager)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();

                var key = Encoding.ASCII.GetBytes(appSettings.Secret);

                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;

                var accountId = jwtToken.Claims.First(x => x.Type == "nameid").Value;
                var role = jwtToken.Claims.First(x => x.Type == "role").Value;

                // attach user to context on successful jwt validation
                var user = await userManager.FindByIdAsync(accountId);
                context.Items["User"] = await userManager.FindByIdAsync(accountId);
                context.Items["Role"] = role;
            }
            catch
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}
