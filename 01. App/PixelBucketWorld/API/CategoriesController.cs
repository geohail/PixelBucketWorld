﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.API.Helpers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers.Mappers.InputModels.InputCategoryModels;
using PixelBucketWorld.Map;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using System.Data;
using System.Threading.Tasks;

namespace PixelBucketWorld.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILoggingService loggingService;

        public CategoriesController(IUnitOfWork unitOfWork, ILoggingService loggingService)
        {
            this.unitOfWork = unitOfWork;
            this.loggingService = loggingService;
        }

        /// <summary>
        /// Organizers can create a new category.
        /// Token authentication.
        /// </summary>
        /// <param name="inputCategory"></param>
        /// <response code="201">Returns the created category and its url.</response>
        /// <response code="400">If name diplactes existing name.</response>
        /// <response code="401">If invalid token.</response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Create([FromBody] InputCategory inputCategory)
        {
            try
            {
                Category category = InputMapper.Map(inputCategory);

                await this.unitOfWork.CategoryService.CreateAsync(category);
                await this.unitOfWork.SaveAsync();

                await this .loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, category);
                await this.unitOfWork.SaveAsync();

                var route = Request.Path.Value;

                string url = UrlHelper.GetUrl(Request, category.Id);

                return Created(url, OutputMapper.Map(category));

            }
            catch (DuplicateNameException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
