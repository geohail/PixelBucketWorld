﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PixeBucketWorld.Data.Models;
using PixeBucketWorld.Data.Models.Logging.Enums;
using PixelBucketWorld.API.Helpers;
using PixelBucketWorld.Consts.Numbers;
using PixelBucketWorld.Consts.Strings;
using PixelBucketWorld.Helpers;
using PixelBucketWorld.Helpers.Mappers.InputModels.InputPhoto;
using PixelBucketWorld.Helpers.Mappers.OutputModels;
using PixelBucketWorld.Helpers.Paging;
using PixelBucketWorld.Services;
using PixelBucketWorld.Services.Contracts;
using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PixelBucketWorld.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ILoggingService loggingService;

        public PhotosController(IUnitOfWork unitOfWork, ILoggingService loggingService)
        {
            this.unitOfWork = unitOfWork;
            this.loggingService = loggingService;
        }

        // api/photos/{id}
        //[Authorize(UserRoleConsts.ORGANISER)]
        /// <summary>
        /// Authenticated users can get photo by id.
        /// Token authentication.
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns the photo.</response>
        /// <response code="204">If photo not found.</response>
        /// <response code="401">If invalid token.</response>
        [Authenticate]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetById(int id)
        {
            // get the active user
            var user = (User)HttpContext.Items["User"];
            var userRole = HttpContext.Items["Role"].ToString();
            try
            {
                var photo = await this.unitOfWork.PhotoService.GetByIdAsync(id, load: true);

                if (userRole == UserRoleConsts.ORGANISER || photo.UserId == user.Id)
                {
                    return Ok(new OutputPhotoFull(photo, Request));
                }
                else
                {
                    return Unauthorized();
                }

            }
            catch (ArgumentNullException)
            {
                return NoContent();
            }

        }

        /// <summary>
        /// Oragnizers can see submitted photos in a particuliar contest.
        /// Token authentication.
        /// </summary>
        /// <param name="contestId"></param>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns a collection of the submitted photos.</response>
        /// <response code="401">If invalid token.</response>
        [Authorize(UserRoleConsts.ORGANISER)]
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> Get([FromQuery] int contestId, [FromQuery] PagingParameters pagingParameters)
        {
            var photos = (await this.unitOfWork.PhotoService.GetAsync(contestId))
                .Select(p => new OutputPhotoPartial(p, Request));

            var pagedPhotos = PagedList<OutputPhotoPartial>.ToPagedList
                (
                photos,
                pagingParameters.Pagenumber,
                pagingParameters.PageSize,
                Request
                );

            WriteHeaders.PagedResposeParameters(Response, pagedPhotos);

            return Ok(pagedPhotos);
        }

        /// <summary>
        /// Anonymous users can get a collection of the latest winning photos.
        /// </summary>
        /// <remarks>
        /// Sample request: GET /api/photos/latestwinners?number=3
        /// </remarks>
        /// <param name="photosNumber"></param>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns the collection of photos</response>
        [HttpGet("latestwinners")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetWinners([FromQuery] int photosNumber, [FromQuery] PagingParameters pagingParameters)
        {
            var photos = (await this.unitOfWork.PhotoService.GetLatestWinningPhotoAsync(photosNumber))
                .Select(p => new OutputPhotoPartial(p, Request));

            var pagedPhotos = PagedList<OutputPhotoPartial>.ToPagedList
                (
                photos,
                pagingParameters.Pagenumber,
                pagingParameters.PageSize,
                Request
                );

            WriteHeaders.PagedResposeParameters(Response, pagedPhotos);

            return Ok(pagedPhotos);

        }

        /// <summary>
        /// Participants can see history of past photos along with their scores and jury coments.
        /// Token authentication.
        /// </summary>
        /// <param name="pagingParameters"></param>
        /// <response code="200">Returns a colletion of photos in finshed contests.</response>
        /// <response code="401">If invalid token.</response>
        [Authenticate]
        [HttpGet("history")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetHistory([FromQuery] PagingParameters pagingParameters)
        {
            var photos1 = (await this.unitOfWork.PhotoService.GetHistoryAsync());
            var photos = photos1
                .Select(p => new OutputPhotoFull(p, Request));

            var pagedPhotos = PagedList<OutputPhotoFull>.ToPagedList
                (
                photos,
                pagingParameters.Pagenumber,
                pagingParameters.PageSize,
                Request
                );

            WriteHeaders.PagedResposeParameters(Response, pagedPhotos);

            return Ok(pagedPhotos);

        }

        /// <summary>
        /// Participants can enroll in contest by uploading a photo along with title and summary.
        /// Token authentication.
        /// </summary>
        /// <response code="201">Returns the uploaded photo model and its url.</response>
        /// <response code="400">
        /// If contest is not in phase I.
        /// If the participant have accepted invitation to be a juror in the contest.
        /// If the contest is invitational and the participant has no invitation or has not accepted it.
        /// If the participant has already uploaded photo.
        /// If the uploaded file does not match the requirements for file extention (.jpg or .png) and file size (up to 2 MB).
        /// </response>
        /// <response code="404">If unexisting contest.</response>
        [Authorize(UserRoleConsts.PARTICIPANT)]
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Create([FromForm] InputPhoto inputPhoto)
        {
            var file = inputPhoto.File;
            var title = inputPhoto.Title;
            var story = inputPhoto.Story;
            var contestId = inputPhoto.ContestId;

            // get the active user
            var user = (User)HttpContext.Items["User"];

            var folderName = Path.Combine("wwwroot", "images", "UploadedPhotos");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fileName =
                Guid.NewGuid().ToString() + "_" + ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

            var filePath = Path.Combine(pathToSave, fileName);
            var dbUrlPath = $"/images/UploadedPhotos/{fileName}";

            // create the photo in database
            Photo photo;
            try
            {
                photo = new Photo()
                {
                    Title = title,
                    Story = story,
                    ContestId = contestId,
                    PhotoUrl = dbUrlPath,
                    UserId = user.Id
                };

                await this.unitOfWork.PhotoService.CreateAsync(photo);

                await Uploader.Upload(file, filePath);

                // CREATE or UPDATE ContestParticipantLink
                var contest = await this.unitOfWork.ContestService.GetByIdAsync(contestId);
                if (contest.TypeId == ContestTypeConsts.OPEN)
                {
                    var cpl = await this.unitOfWork.ContestParticipantLinkService.CreateDefaultContestParticipantLink(user.Id, contestId);
                    this.unitOfWork.ContestParticipantLinkService.UpdateContestInvitationState(cpl);

                }
                else
                {
                    var cpl = await this.unitOfWork.ContestParticipantLinkService.GetAsync(user.Id, contestId);
                    this.unitOfWork.ContestParticipantLinkService.UpdatePhotoUploadingState(cpl);
                }

                await this.unitOfWork.SaveAsync();

                await this.loggingService.LogInformationAsync(InformationLogEvent.INSERT_ITEM, null, photo);
                await this.unitOfWork.SaveAsync();

                var route = Request.Path.Value;

                string url = UrlHelper.GetUrl(Request, photo.Id);

                return Created(url, new OutputPhotoPartial(photo));

            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
