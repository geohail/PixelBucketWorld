﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PixelBucketWorld.API.Helpers
{
    public static class UrlHelper
    {

        public static string GetUrl(HttpRequest request, int id)
        {
            string scheme = request.Scheme;
            string host = request.Host.Value;
            string path = request.Path;
            return $"{scheme}://{host}{path}/{id}";
        }

        public static string GetUrlQuery(HttpRequest request, string suffix)
        {
            string scheme = request.Scheme;
            string host = request.Host.Value;
            string path = request.Path;
            return $"{scheme}://{host}{path}?{suffix}";
        }

    }
}
