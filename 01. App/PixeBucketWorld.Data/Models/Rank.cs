﻿using PixeBucketWorld.Data.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.Models
{
    public class Rank : IUserFilteringCategory
    {
        [Key]
        public int Id { get; set; }

        public int RequiredPoints { get; set; }

        public string Name { get; set; }

        public List<User> Users { get; set; } = new List<User>();
    }
}
