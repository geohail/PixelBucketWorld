﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PixeBucketWorld.Data.Models
{
    public class ContestParticipantLink
    {
        [ForeignKey("UserId")]
        public int UserId { get; set; }

        public User User { get; set; }

        [ForeignKey("ContestId")]
        public int ContestId { get; set; }

        public Contest Contest { get; set; }

        public bool? IsAccepted { get; set; }

        public DateTime? ParticipationDate { get; set; }

        public DateTime? UploadingPhotoTime { get; set; }
    }
}
