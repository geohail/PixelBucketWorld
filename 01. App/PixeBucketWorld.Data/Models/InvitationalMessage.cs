﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PixeBucketWorld.Data.Models
{
    public class InvitationalMessage
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        [ForeignKey("ReceiverId")]
        public int ReceiverId { get; set; }

        public User Receiver { get; set; }

        [ForeignKey("SenderId")]
        public int SenderId { get; set; }

        public User Sender { get; set; }

        [ForeignKey("ContestId")]
        public int ContestId { get; set; }

        public Contest Contest { get; set; }

        public DateTime InvitationDate { get; set; }

        public bool ContestAccepted { get; set; }

        public bool HasBeenViewed { get; set; }
    }
}
