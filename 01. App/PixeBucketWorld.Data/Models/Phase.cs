﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.Models
{
    public class Phase
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Contest> Contests { get; set; } = new List<Contest>();
    }
}
