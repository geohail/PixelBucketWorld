﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.Models.Logging
{
    public class LogEntry
    {
        [Key]
        public int Id { get; set; }

        public string LogLevel { get; set; }

        public int? LogEventId { get; set; }

        public string Message { get; set; }

        public string Details { get; set; } //Stack Trace for Error Log level or Controller name for Information log level 

        public DateTime LogTime { get; set; } 
    }
}
