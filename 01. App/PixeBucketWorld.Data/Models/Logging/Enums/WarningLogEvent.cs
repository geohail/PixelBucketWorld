﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PixeBucketWorld.Data.Models.Logging.Enums
{
    public enum WarningLogEvent
    {
        GET_ITEM_NOT_FOUND = 4000,
        UPDATE_ITEM_NOT_FOUND = 4001
    }
}
