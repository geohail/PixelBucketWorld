﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PixeBucketWorld.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContestTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LogLevel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LogEventId = table.Column<int>(type: "int", nullable: true),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Details = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LogTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Phases",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Phases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ranks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RequiredPoints = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ranks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contests",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    PhaseOneStartingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PhaseOneFinishingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PhaseTwoStartingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PhaseTwoFinishingDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PhaseId = table.Column<int>(type: "int", nullable: false),
                    PhotoUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contests_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contests_ContestTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "ContestTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contests_Phases_PhaseId",
                        column: x => x.PhaseId,
                        principalTable: "Phases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RankId = table.Column<int>(type: "int", nullable: true),
                    CurrentRankingPoints = table.Column<int>(type: "int", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Ranks_RankId",
                        column: x => x.RankId,
                        principalTable: "Ranks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContestJurorLinks",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ContestId = table.Column<int>(type: "int", nullable: false),
                    IsAccepted = table.Column<bool>(type: "bit", nullable: false),
                    AcceptanceTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestJurorLinks", x => new { x.ContestId, x.UserId });
                    table.ForeignKey(
                        name: "FK_ContestJurorLinks_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContestJurorLinks_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContestParticipantLinks",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ContestId = table.Column<int>(type: "int", nullable: false),
                    IsAccepted = table.Column<bool>(type: "bit", nullable: true),
                    ParticipationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UploadingPhotoTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContestParticipantLinks", x => new { x.ContestId, x.UserId });
                    table.ForeignKey(
                        name: "FK_ContestParticipantLinks_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContestParticipantLinks_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvitationalMessages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReceiverId = table.Column<int>(type: "int", nullable: false),
                    SenderId = table.Column<int>(type: "int", nullable: false),
                    ContestId = table.Column<int>(type: "int", nullable: false),
                    InvitationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ContestAccepted = table.Column<bool>(type: "bit", nullable: false),
                    HasBeenViewed = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvitationalMessages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InvitationalMessages_AspNetUsers_ReceiverId",
                        column: x => x.ReceiverId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvitationalMessages_AspNetUsers_SenderId",
                        column: x => x.SenderId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvitationalMessages_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Photos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Story = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PhotoUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ContestId = table.Column<int>(type: "int", nullable: false),
                    ContestRanking = table.Column<int>(type: "int", nullable: true),
                    FinalPoints = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Photos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Photos_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Photos_Contests_ContestId",
                        column: x => x.ContestId,
                        principalTable: "Contests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhotoAssessments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Score = table.Column<int>(type: "int", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    PhotoId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhotoAssessments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhotoAssessments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PhotoAssessments_Photos_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "Photos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "430ef829-5787-47d8-ad55-ad89c047f375", "Organiser", "ORGANISER" },
                    { 2, "60fc0de8-871a-41c8-a1a3-8248c750eff1", "Participant", "PARTICIPANT" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CurrentRankingPoints", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "Password", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "RankId", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { 1, 0, "9d281142-44cb-4dd0-b0ae-9c37b64d7d64", null, "mihailpopov@abv.bg", false, "Mihail", "Popov", false, null, "MIHAILPOPOV@ABV.BG", "MIHAILPOPOV", null, "AQAAAAEAACcQAAAAEBRrIRD3TWANrpBbxv9nRk7DjxPasxeqqvzLpA8V6R6cv+aW2vGizYHFSzycro7UKg==", null, false, null, "e2a45d86-0cd6-4579-8eca-1e8d50eae5b0", false, "mihailpopov" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 5, "Faces" },
                    { 1, "Animals" },
                    { 3, "Boats" },
                    { 2, "Nature" },
                    { 4, "Bridges" }
                });

            migrationBuilder.InsertData(
                table: "ContestTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Open" },
                    { 2, "Invitational" }
                });

            migrationBuilder.InsertData(
                table: "Phases",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Prestart" },
                    { 2, "Phase I" },
                    { 4, "Phase II" },
                    { 5, "Finished" },
                    { 3, "Transition" }
                });

            migrationBuilder.InsertData(
                table: "Ranks",
                columns: new[] { "Id", "Name", "RequiredPoints" },
                values: new object[,]
                {
                    { 1, "Junkie", 0 },
                    { 2, "Enthusiast", 51 },
                    { 3, "Master", 151 },
                    { 4, "Wise and Benevolent Photo Dictator", 1001 }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CurrentRankingPoints", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "Password", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "RankId", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 2, 0, "d274f1e2-8465-4df3-ab86-099584009f04", 0, "georgistefanov@abv.bg", false, "Georgi", "Stefanov", false, null, "GEORGISTEFANOV@ABV.BG", "GEORGISTEFANOV", null, "AQAAAAEAACcQAAAAEHKVzX0vrt8//Iqcw/1I5M38fpVUhfX3dRBAIgLMacrHK8lhgIbU4BF+sHBw6QPNkQ==", null, false, 1, "1e5481ed-5fd9-4c6a-8532-dd77026a4860", false, "georgistefanov" },
                    { 6, 0, "ee50739d-e3e0-48a4-8186-df31320c257b", 45, "flint@gmail.com", false, "Captain", "Flint", false, null, "FLINT@GMAIL.COM", "CAPTAINFLINT", null, "AQAAAAEAACcQAAAAEOOhEXMvY3jUpqU4MWRxuqZDvJIKbVAr1qp4RsOVzzXP2hKpcP36cRAmLaOJQgT0CA==", null, false, 1, "ecdfcf05-f594-4e20-85b1-8996f0316bf7", false, "CaptainFlint" },
                    { 3, 0, "2874014e-ffb0-4d10-b911-b28427a16d63", 60, "bengunn@gmail.com", false, "Ben", "Gunn", false, null, "BENGUNN@GMAIL.COM", "BENGUNN", null, "AQAAAAEAACcQAAAAEL2XQ0CvpGc77Ad72qnlgRmQJG7q3GTECw9jSKS0Z3iQE2JvWlzRvJUlPI2E/JgoRQ==", null, false, 2, "0f5e5730-0fa4-47ab-8c5d-dedab1faed06", false, "bengunn" },
                    { 4, 0, "0a73dd47-15e8-4dd8-83f0-c82a30b219e2", 900, "johnsilver@gmail.com", false, "John", "Silver", false, null, "JOHNSILVER@GMAIL.COM", "LONGJOHNSILVER", null, "AQAAAAEAACcQAAAAEPkIarzMmHC3tb3MarAEfN5pux98ZORji1ID/+L26BOhx4rVCIdTFFK8JhkURms57A==", null, false, 3, "08e5e244-4437-47be-b31c-3ab066593f96", false, "longjohnsilver" },
                    { 5, 0, "d03f1f4b-d687-413d-8b32-96fd419944dc", 2000, "treasure@gmail.com", false, "Jim", "Hawkins", false, null, "TREASURE@GMAIL.COM", "JIMHAWKINS", null, "AQAAAAEAACcQAAAAEHXtzkkD/iYyVK7Axnwds9C/UnknX+LMXvZG5L+J+AmMq3azh6JFCX1o7rtmnnM/3A==", null, false, 4, "f9fed26d-7066-40ed-8e98-1a447a749187", false, "jimhawkins" }
                });

            migrationBuilder.InsertData(
                table: "Contests",
                columns: new[] { "Id", "CategoryId", "Name", "PhaseId", "PhaseOneFinishingDate", "PhaseOneStartingDate", "PhaseTwoFinishingDate", "PhaseTwoStartingDate", "PhotoUrl", "TypeId" },
                values: new object[,]
                {
                    { 4, 1, "Animals in the wild - 2", 1, new DateTime(2021, 6, 12, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9564), new DateTime(2021, 6, 11, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9561), new DateTime(2021, 6, 14, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9570), new DateTime(2021, 6, 13, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9567), "/images/ContestPhotos/PixelBucketWorld.jpg", 2 },
                    { 1, 1, "Cuttest Animals", 2, new DateTime(2021, 6, 10, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9508), new DateTime(2021, 6, 8, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9505), new DateTime(2021, 6, 13, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9515), new DateTime(2021, 6, 12, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9512), "/images/ContestPhotos/Anymals.jpg", 1 },
                    { 2, 1, "Animals in the wild", 2, new DateTime(2021, 6, 11, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9527), new DateTime(2021, 6, 7, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9523), new DateTime(2021, 6, 13, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9533), new DateTime(2021, 6, 12, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9530), "/images/ContestPhotos/Anymals.jpg", 2 },
                    { 3, 2, "Motorcicle touring", 4, new DateTime(2021, 6, 7, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9550), new DateTime(2021, 6, 6, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9546), new DateTime(2021, 6, 10, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9556), new DateTime(2021, 6, 9, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9553), "/images/ContestPhotos/Nature.jpg", 1 },
                    { 101, 3, "Small Boats", 5, new DateTime(2021, 5, 31, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(6581), new DateTime(2021, 5, 30, 10, 15, 5, 705, DateTimeKind.Local).AddTicks(8721), new DateTime(2021, 6, 2, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(8047), new DateTime(2021, 6, 1, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(7390), "/images/ContestPhotos/Boats.jpg", 1 },
                    { 102, 4, "Long Bridges", 5, new DateTime(2021, 5, 31, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9437), new DateTime(2021, 5, 30, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9389), new DateTime(2021, 6, 2, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9466), new DateTime(2021, 6, 1, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9453), "/images/ContestPhotos/Bridges.jpg", 1 },
                    { 103, 5, "One Human Face", 5, new DateTime(2021, 5, 31, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9493), new DateTime(2021, 5, 30, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9490), new DateTime(2021, 6, 2, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9501), new DateTime(2021, 6, 1, 10, 15, 5, 709, DateTimeKind.Local).AddTicks(9497), "/images/ContestPhotos/PixelBucketWorld.jpg", 1 }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { 2, 2 },
                    { 2, 3 },
                    { 2, 5 },
                    { 2, 4 },
                    { 2, 6 }
                });

            migrationBuilder.InsertData(
                table: "ContestJurorLinks",
                columns: new[] { "ContestId", "UserId", "AcceptanceTime", "IsAccepted" },
                values: new object[,]
                {
                    { 3, 5, null, true },
                    { 3, 4, null, false }
                });

            migrationBuilder.InsertData(
                table: "ContestParticipantLinks",
                columns: new[] { "ContestId", "UserId", "IsAccepted", "ParticipationDate", "UploadingPhotoTime" },
                values: new object[,]
                {
                    { 103, 3, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8640), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8644) },
                    { 1, 3, null, new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8666), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8669) },
                    { 2, 3, true, new DateTime(2021, 6, 8, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8673), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8676) },
                    { 3, 6, null, new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8708), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8711) },
                    { 103, 6, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8648), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8652) },
                    { 102, 6, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8626), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8629) },
                    { 101, 6, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8600), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8603) },
                    { 102, 3, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8615), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8619) },
                    { 101, 3, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8561), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8586) },
                    { 3, 4, null, new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8701), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8704) },
                    { 3, 2, null, new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8688), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8691) },
                    { 2, 2, true, new DateTime(2021, 6, 8, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8680), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8684) },
                    { 1, 2, null, new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8658), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8662) },
                    { 103, 2, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8632), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8637) },
                    { 102, 2, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8608), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8611) },
                    { 101, 2, null, new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(7077), new DateTime(2021, 5, 30, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(7848) },
                    { 3, 3, null, new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8695), new DateTime(2021, 6, 9, 10, 15, 5, 770, DateTimeKind.Local).AddTicks(8698) }
                });

            migrationBuilder.InsertData(
                table: "InvitationalMessages",
                columns: new[] { "Id", "Content", "ContestAccepted", "ContestId", "HasBeenViewed", "InvitationDate", "ReceiverId", "SenderId", "Title" },
                values: new object[,]
                {
                    { 3, "Congratulations longjohnsilver! You have been invited to be a juror for the new contest Motorcicle touring", false, 3, true, new DateTime(2021, 6, 6, 10, 15, 5, 771, DateTimeKind.Local).AddTicks(4618), 4, 1, "Contest juror invitation!" },
                    { 1, "Congratulations bengunn! You have been invited to participate in the new contest Anymals in the wild", true, 2, true, new DateTime(2021, 6, 7, 10, 15, 5, 771, DateTimeKind.Local).AddTicks(2942), 3, 1, "Contest participation invitation!" },
                    { 4, "Congratulations jimhawkins! You have been invited to be a juror for the new contest Motorcicle touring", true, 3, true, new DateTime(2021, 6, 6, 10, 15, 5, 771, DateTimeKind.Local).AddTicks(4624), 5, 1, "Contest juror invitation!" },
                    { 2, "Congratulations georgistefanov! You have been invited to participate in the new contest Anymals in the wild", true, 2, true, new DateTime(2021, 6, 7, 10, 15, 5, 771, DateTimeKind.Local).AddTicks(4576), 2, 1, "Contest participation invitation!" }
                });

            migrationBuilder.InsertData(
                table: "Photos",
                columns: new[] { "Id", "ContestId", "ContestRanking", "FinalPoints", "PhotoUrl", "Story", "Title", "UserId" },
                values: new object[,]
                {
                    { 203, 102, 3, 8.0, "/images/UploadedPhotos/pic102.3.jpg", "The bridges of the county are hewn from granite in our mountains. They have stood strong for generations, linking the folks in these parts, giving the sweetest of spots for romance and reunions.", "And another one bridge", 6 },
                    { 7, 3, null, 0.0, "/images/UploadedPhotos/pic3.3.jpg", "We've put together a complete guide with all the motorcycle touring tips and tricks you need to know.", "Motorcycle Touring Tips", 4 },
                    { 101, 101, 1, 10.0, "/images/UploadedPhotos/pic101.1.jpg", "A cleverly crafted phrase like Black Pearl or Bankers Hours makes a statement without having to say to much.", "A boat", 2 },
                    { 201, 102, 1, 10.0, "/images/UploadedPhotos/pic102.1.jpg", "Free running water under bridges of golden rock, those are the bridges that call us all home in our evening years.", "A bridge", 2 },
                    { 301, 103, 1, 10.0, "/images/UploadedPhotos/pic103.1.jpg", "grayscale photography, man, looking, person, human, face, portrait, close, black and white, fold, age, male, adult, senior adult, headshot, one person", "A face", 2 },
                    { 1, 1, null, 0.0, "/images/UploadedPhotos/pic1.1.jpg", "I just saw them in the forest.", "Two little bears", 2 },
                    { 6, 3, null, 0.0, "/images/UploadedPhotos/pic3.2.jpg", "TOURING NORWAY BY MOTORCYCLE", "BIKER’S BUCKET LIST", 3 },
                    { 3, 2, null, 0.0, "/images/UploadedPhotos/pic2.1.jpg", "Killer?", "Killer whales", 3 },
                    { 2, 1, null, 0.0, "/images/UploadedPhotos/pic1.2.jpg", "Ready for the party ....", "Pinguin meeting on the ice", 3 },
                    { 302, 103, 2, 9.0, "/images/UploadedPhotos/pic103.2.jpg", "...so many strange contrasts in one human face...", "Another face", 3 },
                    { 202, 102, 2, 9.0, "/images/UploadedPhotos/pic102.2.jpg", "The bridges are marvels of engineering, yet of pale beauty by comparison to our river.", "Another bridge", 3 },
                    { 102, 101, 2, 9.0, "/images/UploadedPhotos/pic101.2.jpg", "Whether you’ve hopped on board a super-yacht or are sailing around on a sunfish– there’s nothing better than hitting the high seas.", "Another boat", 3 },
                    { 4, 2, null, 0.0, "/images/UploadedPhotos/pic2.2.jpg", "I dropped it in my pants and run away.", "Mama bear", 2 },
                    { 5, 3, null, 0.0, "/images/UploadedPhotos/pic3.1.jpg", "The freedom of being on a bike is bliss isn’t it? You have a full 360 degree Panoramic view of your environment and you get to your destination far more relaxed than you would in a car. ", "5 Reasons Why You Should Travel With A Motorcycle", 2 }
                });

            migrationBuilder.InsertData(
                table: "Photos",
                columns: new[] { "Id", "ContestId", "ContestRanking", "FinalPoints", "PhotoUrl", "Story", "Title", "UserId" },
                values: new object[] { 103, 101, 3, 8.0, "/images/UploadedPhotos/pic101.3.jpg", "From the iconic names of super-yachts to sea-worthy puns, we’ve rounded up the most chic, popular, and all around classic handles to give you the best boat names of 2019.", "And another one boat", 6 });

            migrationBuilder.InsertData(
                table: "Photos",
                columns: new[] { "Id", "ContestId", "ContestRanking", "FinalPoints", "PhotoUrl", "Story", "Title", "UserId" },
                values: new object[] { 303, 103, 3, 8.0, "/images/UploadedPhotos/pic103.3.jpg", "Human face can be different with strong expressions and emotions. Funny, positive, squinting with one eye man. Facial expression, emotions.", "And another one face", 6 });

            migrationBuilder.InsertData(
                table: "Photos",
                columns: new[] { "Id", "ContestId", "ContestRanking", "FinalPoints", "PhotoUrl", "Story", "Title", "UserId" },
                values: new object[] { 8, 3, null, 0.0, "/images/UploadedPhotos/pic3.4.jpg", "Quite simply, it’s the riding. The Iberian peninsula is huge, with every kind of landscape you could imagine – from verdant Galicia on the north-west Atlantic coast to sun-baked Murcia on the Mediterranean.", "Motorcycle Touring in Spain and Portugal", 6 });

            migrationBuilder.InsertData(
                table: "PhotoAssessments",
                columns: new[] { "Id", "Comment", "PhotoId", "Score", "UserId" },
                values: new object[,]
                {
                    { 101, "Best photo!", 101, 10, 1 },
                    { 201, "Best photo!", 201, 10, 1 },
                    { 301, "Best photo!", 301, 10, 1 },
                    { 1, "Best photo!", 5, 10, 1 },
                    { 103, "Not a bad photo!", 103, 8, 1 },
                    { 203, "Not a bad photo!", 203, 8, 1 },
                    { 303, "Not a bad photo!", 303, 8, 1 },
                    { 4, "You need more experience!", 8, 3, 5 },
                    { 102, "Good photo!", 102, 9, 1 },
                    { 202, "Good photo!", 202, 9, 1 },
                    { 302, "Good photo!", 302, 9, 1 },
                    { 2, "Good job!", 6, 8, 5 },
                    { 3, "Interesting!", 7, 6, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_RankId",
                table: "AspNetUsers",
                column: "RankId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_UserName",
                table: "AspNetUsers",
                column: "UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Name",
                table: "Categories",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContestJurorLinks_UserId",
                table: "ContestJurorLinks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ContestParticipantLinks_UserId",
                table: "ContestParticipantLinks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Contests_CategoryId",
                table: "Contests",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Contests_Name",
                table: "Contests",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contests_PhaseId",
                table: "Contests",
                column: "PhaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Contests_TypeId",
                table: "Contests",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_InvitationalMessages_ContestId",
                table: "InvitationalMessages",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_InvitationalMessages_ReceiverId",
                table: "InvitationalMessages",
                column: "ReceiverId");

            migrationBuilder.CreateIndex(
                name: "IX_InvitationalMessages_SenderId",
                table: "InvitationalMessages",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAssessments_PhotoId",
                table: "PhotoAssessments",
                column: "PhotoId");

            migrationBuilder.CreateIndex(
                name: "IX_PhotoAssessments_UserId",
                table: "PhotoAssessments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_ContestId",
                table: "Photos",
                column: "ContestId");

            migrationBuilder.CreateIndex(
                name: "IX_Photos_UserId",
                table: "Photos",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ContestJurorLinks");

            migrationBuilder.DropTable(
                name: "ContestParticipantLinks");

            migrationBuilder.DropTable(
                name: "InvitationalMessages");

            migrationBuilder.DropTable(
                name: "Logs");

            migrationBuilder.DropTable(
                name: "PhotoAssessments");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Photos");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Contests");

            migrationBuilder.DropTable(
                name: "Ranks");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "ContestTypes");

            migrationBuilder.DropTable(
                name: "Phases");
        }
    }
}
