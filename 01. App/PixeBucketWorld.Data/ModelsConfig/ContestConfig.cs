﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PixeBucketWorld.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PixeBucketWorld.Data.ModelsConfig
{
    class ContestConfig : IEntityTypeConfiguration<Contest>
    {
        public void Configure(EntityTypeBuilder<Contest> entity)
        {
            entity.HasIndex(c => c.Name).IsUnique();
        }
    }
}
