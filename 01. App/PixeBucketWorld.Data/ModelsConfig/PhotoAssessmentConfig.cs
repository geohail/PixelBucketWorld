﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PixeBucketWorld.Data.Models;

namespace PixeBucketWorld.Data.ModelsConfig
{
    class PhotoAssessmentConfig : IEntityTypeConfiguration<PhotoAssessment>
    {
        public void Configure(EntityTypeBuilder<PhotoAssessment> builder)
        {
            builder.HasOne(pa => pa.Photo).WithMany(p => p.PhotoAssessments);
        }
    }
}
