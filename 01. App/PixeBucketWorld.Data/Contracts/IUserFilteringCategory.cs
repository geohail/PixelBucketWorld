﻿namespace PixeBucketWorld.Data.Contracts
{
    public interface IUserFilteringCategory
    {
        int Id { get; set; }

        string Name { get; set; }
    }
}
