﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.CustomDataAnnotations
{
    public class PhaseOneTimeInterval : ValidationAttribute
    {
        private readonly int days;

        public PhaseOneTimeInterval(int days)
        {
            this.days = days;
            base.ErrorMessage = $"Phase one time interval must be no more than {days} days";
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var phaseOneFinishingDate = (DateTime)value;

            var phaseOneStartingDate = (DateTime)validationContext
                .ObjectInstance
                .GetType()
                .GetProperty("PhaseOneStartingDate")
                .GetValue(validationContext.ObjectInstance);

            if((phaseOneFinishingDate-phaseOneStartingDate).TotalDays <= days)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(base.ErrorMessage);
        }
    }
}
