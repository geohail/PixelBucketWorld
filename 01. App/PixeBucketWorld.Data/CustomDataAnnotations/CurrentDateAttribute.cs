﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PixeBucketWorld.Data.ValidationAttributes
{
    public class CurrentDateAttribute : ValidationAttribute
    {
        public CurrentDateAttribute(string errorMessage) 
            : base(errorMessage)
        {
        }

        public override bool IsValid(object value)
        {
            var dt = (DateTime)value;

            if (dt >= DateTime.Now)
            {
                return true;
            }

            return false;
        }
    }
}
