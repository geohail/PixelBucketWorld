# **PixelBucketWorld**

### **GENERAL**

#### **Overall funcional description for users**
"PixelBucketWorld" is an application for organizing and managing online photo contests.

The application works with three types of users:
- `anonimous` - any unregisyered user;
- `participant` - a registered user that has authenticated;
- `organizer` - an application owner that has authenticated;

Participants have ranks depending on the accumulated points in contests:
- `junkie` - 0-50 points;
- `enthusiast` - 51-150 points;
- `master` - 151-1000 points;
- wise and benevolent photo dictator (`dictator`) - 1001 and over points.

A contest jury is composed as a minimum of all organizers. Participants with rank master or dictator could take part in the jury if they have been invited.

Contests may be of two types:
- `open` - any participant could race;
- `invitational` - only invited participants could race;

The generic flow of a contest is as follows:
- `Phase prestart:` Organizers start a contest and send invitations to participants if the contest is invitational and optionally to masters and dictators to take part in the jury. Invitations could be accepted from the invited only that and the next phase. If a participant have accepted invitation for jurer he can no longer take part in the contest as racer.
- `Phase I:` Participants that want to take part in the contest upload photos along with title and story. There are limitations for file extension (.jpg, .png) and for file-size (2 MB).) Oragnizers can only review the oploaded photos. Photos can be uploaded only in that phase.
- `Phase transitional:` A transition fase between phase I and phase II.
- `Phase II:` Jurers give assessments to the uploaded photos giving them score from 0 to 10 points and a comment. Zero points could are given if the photo is not relevant to the contest category.
- `Phase finished:` Photos are classified on the basis of their total score and the first three winners are determined. Final ponits are calculated for the uploaded photos. In that phase participants can review the uploaded photos along with the photo-assesments.

Anonimous users can see only a few of the latest winning photos.

#### **Application description**
The above functionaluty is provided simultaneously through ASP.NET Core MVC and API. The data is stored оn MS SQL Server. The application maintains multi-layered architecture with three main layers - presentation (views), business (services) and persistance (data).

`PixelBucketWorld`
This is the main project containing the API and MVC parts. It contains the logic related to communicating with the client and sending orders to the services. MVC controllers are organized in a separate area Areas/PBW in order to achieve separation from API-controllers.
The project contains the IoC container providing the required instantions of services.
There are two custom middleares:
- Exception handler middleware implemented to chatch any unhandlned exceptions. If happen these are logged in the database;
- Jwt middleware implemented to authenticate the API-clients through authentication token.

`PixelBucketWorld.Data`
Contains the data models in relation to communication with the database.

`PixelBucketWorld.Service`
Intermediate layer to separate presentation and data layers. This project provides the business logic (services) needed for the execution of orders sent from the controllers. Repository and Unit Of Work Design Patterns are applied to the services as follows:
- All services are provided by UnitOfWork class and its interface. The UnitOfWork is instantinated by IoC container. The controllers in presentation layer work with that instance.
- There is a generic service class containing tha basic service methods for CRUD operations. This class is inherited by every concrete service implementing its basic methods and providing additional logic by overriding the base methods or by new methods.
- There is a separate service for logging important events. It is instantinated by IoC container.

There a timed hosted service in a separate project that is responsible for switching the contest phases and of any constest switches to phase "finished" to perform the finalization of the project calling the responcible services. 

### **Web-site and MVC**
MVC controllers with views are implemented to reneder the data on the web-site.

### **API**
Web API controllers are implemented to communicate with clients. For private resources initial POST request is required with username and password in the Authorization request-header. Provide them as Base64 encoded string "username:password". If credentials are valid then API returns authentication token that could be used in next requstes. The token is inserted in the request Authorization header as string. A token contains information for the user id and its role. A token is valid for limited time of 7 days.

Typically:
- Collections are returned paginated and the responce header contains information about the total pages, total records and the URLs of the previous and next pages if any.
- Requests for creating entities produce responce containing the created entity and the its URL in the responce header.
- The input/output entities are DTOs. Database models are not exposed to the client.


## **INSTRUCTIONS FOR BUILDING AND RUNNING THE PROJECT**

1.	Download folder PixelBucketWorld.
2.	Run the file PixelBucketWorld.sln in Visual Studio.
3.	In menu Tools select NuGet Packager Manager then Package Manager Console (PMC).
4.	In PMC type the commad "update-database" to build the database on SQL Server and load it with initial data. Use server name .\SQLEXPRESS when connecting to SQL server.
5.	Run the project - PixelBucketWorld.
6.	The browser will run and the web-site will load for browsing. Http-requests to API can be made in [Swagger](https://localhost:5001/swagger/index.html).


## **DATABASE DIAGRAM**

Check our [database diagram](https://gitlab.com/geohail/PixelBucketWorld/-/blob/master/02.%20Database%20diagram/PixwlBucketWorld.DatabaseDiagram.png)

## **OTHER LINKS**

Visit our [Trello board]( https://trello.com/b/KC6WPEeW/pixelbucketworld#)